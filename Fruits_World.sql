USE [master]
GO
/****** Object:  Database [Fruits_World]    Script Date: 3/26/2022 10:21:27 AM ******/
CREATE DATABASE [Fruits_World]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Fruits_World', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Fruits_World.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Fruits_World_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Fruits_World_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Fruits_World] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Fruits_World].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Fruits_World] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Fruits_World] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Fruits_World] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Fruits_World] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Fruits_World] SET ARITHABORT OFF 
GO
ALTER DATABASE [Fruits_World] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Fruits_World] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Fruits_World] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Fruits_World] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Fruits_World] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Fruits_World] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Fruits_World] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Fruits_World] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Fruits_World] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Fruits_World] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Fruits_World] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Fruits_World] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Fruits_World] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Fruits_World] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Fruits_World] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Fruits_World] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Fruits_World] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Fruits_World] SET RECOVERY FULL 
GO
ALTER DATABASE [Fruits_World] SET  MULTI_USER 
GO
ALTER DATABASE [Fruits_World] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Fruits_World] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Fruits_World] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Fruits_World] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Fruits_World] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Fruits_World] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Fruits_World', N'ON'
GO
ALTER DATABASE [Fruits_World] SET QUERY_STORE = OFF
GO
USE [Fruits_World]
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[ID_HoaDon] [int] NULL,
	[ID_SanPham] [int] NULL,
	[SoLuongMua(KG)] [int] NULL,
	[GiaMua(VND)] [int] NULL,
	[isFeedback] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanhGia]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhGia](
	[ID_TaiKhoan] [int] NULL,
	[ID_SanPham] [int] NULL,
	[SaoCuaSanPham] [int] NULL,
	[DanhGiaSanPham] [nvarchar](max) NULL,
	[NgayDanhGia] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[ID_HoaDon] [int] IDENTITY(1,1) NOT NULL,
	[ID_TaiKhoanCus] [int] NULL,
	[ID_TaiKhoanSale] [int] NULL,
	[NgayLapHoaDon] [datetime] NULL,
	[ID_TrangThaiDonHang] [int] NULL,
	[ID_PhuongThucThanhToan] [int] NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[ID_HoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LichSuTimKiem]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LichSuTimKiem](
	[ID_TimKiem] [int] IDENTITY(1,1) NOT NULL,
	[ID_TaiKhoan] [int] NULL,
	[NoiDung] [nvarchar](max) NULL,
 CONSTRAINT [PK_LichSuTimKiem] PRIMARY KEY CLUSTERED 
(
	[ID_TimKiem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhuongThucThanhToan]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhuongThucThanhToan](
	[ID_PhuongThucThanhToan] [int] NOT NULL,
	[TenPhuongThucThanhToan] [nvarchar](max) NULL,
 CONSTRAINT [PK_PhuongThucThanhToan] PRIMARY KEY CLUSTERED 
(
	[ID_PhuongThucThanhToan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID_Role] [int] NOT NULL,
	[TenRole] [nvarchar](max) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID_Role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[ID_SanPham] [int] IDENTITY(1,1) NOT NULL,
	[ID_TaiKhoan] [int] NULL,
	[TenSanPham] [nvarchar](max) NULL,
	[AnhSanPham] [nvarchar](max) NULL,
	[ThongTinSanPham] [nvarchar](max) NULL,
	[GiaSanPham(VND)] [int] NULL,
	[SoLuongSanPham] [int] NULL,
	[ID_TrangThaiSanPham] [int] NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[ID_SanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SanPhamTrongGioHang]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPhamTrongGioHang](
	[ID_TaiKhoan] [int] NULL,
	[ID_SanPham] [int] NULL,
	[SoLuong(KG)] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThongBao]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongBao](
	[ID_TaiKhoan] [int] NOT NULL,
	[NoiDung] [nvarchar](max) NULL,
	[NgayThongBao] [datetime] NULL,
	[isWatch] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThongTinTaiKhoan]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongTinTaiKhoan](
	[ID_TaiKhoan] [int] IDENTITY(1,1) NOT NULL,
	[TaiKhoan] [nvarchar](max) NULL,
	[MatKhau] [nvarchar](max) NULL,
	[Ten] [nvarchar](max) NULL,
	[NamSinh] [int] NULL,
	[SDT] [nvarchar](max) NULL,
	[DiaChi] [nvarchar](max) NULL,
	[Tien] [int] NULL,
	[ID_Role] [int] NULL,
	[isDelete] [bit] NULL,
	[Avt] [nvarchar](max) NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[ID_TaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrangThaiDonHang]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrangThaiDonHang](
	[ID_TrangThaiDonHang] [int] NOT NULL,
	[TenTrangThaiDonHang] [nvarchar](max) NULL,
 CONSTRAINT [PK_TrangThaiDonHang] PRIMARY KEY CLUSTERED 
(
	[ID_TrangThaiDonHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrangThaiSanPham]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrangThaiSanPham](
	[ID_TrangThaiSanPham] [int] NOT NULL,
	[Ten_TrangThaiSanPham] [nvarchar](max) NULL,
 CONSTRAINT [PK_TrangThaiSanPham] PRIMARY KEY CLUSTERED 
(
	[ID_TrangThaiSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (25, 1, 13, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (25, 6, 1, 20000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (26, 27, 8, 45000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (26, 26, 1, 25000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (27, 27, 1, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (30, 27, 12, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (28, 6, 1, 20000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (29, 1, 1, 50000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (24, 6, 1, 20000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (31, 22, 400, 500000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (32, 27, 6, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (33, 1, 1, 50000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (34, 26, 1, 25000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (35, 18, 1, 35000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (45, 1, 70, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (46, 27, 85, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (36, 9, 1, 30000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (37, 27, 1, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (38, 6, 1, 20000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (39, 1, 20, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (40, 26, 10, 25000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (41, 1, 43, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (42, 1, 7, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (43, 26, 1, 25000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (44, 6, 1, 20000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (47, 14, 1, 15000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (48, 9, 1, 30000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (49, 15, 1, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (50, 27, 1, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (51, 25, 1, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (51, 29, 1, 30000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (52, 22, 1, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (55, 29, 100, 30000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (56, 1, 1, 50000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (57, 29, 1, 30000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (53, 27, 2, 45000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (54, 27, 11, 45000, 1)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (58, 9, 5, 30000, 0)
INSERT [dbo].[ChiTietHoaDon] ([ID_HoaDon], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)], [isFeedback]) VALUES (59, 29, 10, 30000, 0)
GO
INSERT [dbo].[DanhGia] ([ID_TaiKhoan], [ID_SanPham], [SaoCuaSanPham], [DanhGiaSanPham], [NgayDanhGia]) VALUES (6, 1, 5, N'Sầu riêng quá ngon khiến tôi gục ngã', CAST(N'2022-03-25T11:11:35.100' AS DateTime))
INSERT [dbo].[DanhGia] ([ID_TaiKhoan], [ID_SanPham], [SaoCuaSanPham], [DanhGiaSanPham], [NgayDanhGia]) VALUES (6, 26, 5, N'Ngon quá trời', CAST(N'2022-03-25T11:12:43.337' AS DateTime))
INSERT [dbo].[DanhGia] ([ID_TaiKhoan], [ID_SanPham], [SaoCuaSanPham], [DanhGiaSanPham], [NgayDanhGia]) VALUES (6, 27, 5, N'Ngon quá đi', CAST(N'2022-03-25T11:14:57.617' AS DateTime))
INSERT [dbo].[DanhGia] ([ID_TaiKhoan], [ID_SanPham], [SaoCuaSanPham], [DanhGiaSanPham], [NgayDanhGia]) VALUES (5, 9, 5, N'Qúa trời ngon ', CAST(N'2022-03-25T21:24:28.623' AS DateTime))
INSERT [dbo].[DanhGia] ([ID_TaiKhoan], [ID_SanPham], [SaoCuaSanPham], [DanhGiaSanPham], [NgayDanhGia]) VALUES (16, 27, 5, N'Rất ngon', CAST(N'2022-03-26T08:19:57.490' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[HoaDon] ON 

INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (24, 5, 5, CAST(N'2022-03-22T22:27:17.037' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (25, 6, 5, CAST(N'2021-01-01T00:00:00.000' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (26, 6, 12, CAST(N'2022-03-22T23:03:53.753' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (27, 6, 12, CAST(N'2022-03-22T23:18:21.617' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (28, 6, 5, CAST(N'2022-03-22T23:18:32.037' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (29, 6, 5, CAST(N'2022-02-01T00:00:00.000' AS DateTime), 3, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (30, 6, 12, CAST(N'2022-03-22T23:19:19.740' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (31, 5, 12, CAST(N'2022-03-24T11:19:01.883' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (32, 5, 12, CAST(N'2022-03-24T14:31:29.453' AS DateTime), 4, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (33, 5, 5, CAST(N'2022-03-24T21:36:23.163' AS DateTime), 3, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (34, 5, 12, CAST(N'2022-03-24T21:37:23.237' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (35, 5, 5, CAST(N'2022-03-24T21:37:44.153' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (36, 5, 5, CAST(N'2022-03-24T21:52:03.180' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (37, 5, 12, CAST(N'2022-03-24T21:52:03.187' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (38, 5, 5, CAST(N'2022-03-24T21:52:15.000' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (39, 6, 5, CAST(N'2022-03-25T10:51:58.020' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (40, 6, 12, CAST(N'2022-03-25T10:51:58.023' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (41, 6, 5, CAST(N'2022-03-25T10:55:29.770' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (42, 6, 5, CAST(N'2022-03-25T10:55:58.700' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (43, 6, 12, CAST(N'2022-03-25T10:56:12.387' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (44, 6, 5, CAST(N'2022-03-25T10:56:32.713' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (45, 6, 5, CAST(N'2022-03-25T10:58:54.477' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (46, 6, 12, CAST(N'2022-03-25T11:08:10.117' AS DateTime), 3, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (47, 5, 5, CAST(N'2022-03-26T07:30:21.127' AS DateTime), 1, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (48, 5, 5, CAST(N'2022-03-26T07:32:32.430' AS DateTime), 1, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (49, 5, 5, CAST(N'2022-03-26T07:34:53.923' AS DateTime), 1, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (50, 5, 12, CAST(N'2022-03-26T07:35:01.947' AS DateTime), 1, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (51, 5, 12, CAST(N'2022-03-26T07:35:49.400' AS DateTime), 1, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (52, 5, 12, CAST(N'2022-03-26T07:36:05.703' AS DateTime), 1, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (53, 16, 12, CAST(N'2022-03-26T08:12:45.760' AS DateTime), 4, 2)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (54, 16, 12, CAST(N'2022-03-26T08:15:12.110' AS DateTime), 3, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (55, 16, 12, CAST(N'2022-03-26T08:31:13.583' AS DateTime), 4, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (56, 16, 5, CAST(N'2022-03-26T08:33:23.300' AS DateTime), 3, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (57, 16, 12, CAST(N'2022-03-26T08:33:23.320' AS DateTime), 4, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (58, 16, 5, CAST(N'2022-03-26T10:16:11.770' AS DateTime), 1, 1)
INSERT [dbo].[HoaDon] ([ID_HoaDon], [ID_TaiKhoanCus], [ID_TaiKhoanSale], [NgayLapHoaDon], [ID_TrangThaiDonHang], [ID_PhuongThucThanhToan]) VALUES (59, 16, 12, CAST(N'2022-03-26T10:16:11.780' AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[HoaDon] OFF
GO
INSERT [dbo].[PhuongThucThanhToan] ([ID_PhuongThucThanhToan], [TenPhuongThucThanhToan]) VALUES (1, N'Thanh Toán Tiền Mặt')
INSERT [dbo].[PhuongThucThanhToan] ([ID_PhuongThucThanhToan], [TenPhuongThucThanhToan]) VALUES (2, N'Thanh Toán Qua Bank')
GO
INSERT [dbo].[Role] ([ID_Role], [TenRole]) VALUES (0, N'Admin')
INSERT [dbo].[Role] ([ID_Role], [TenRole]) VALUES (1, N'Seller')
INSERT [dbo].[Role] ([ID_Role], [TenRole]) VALUES (2, N'RegisteredSell')
INSERT [dbo].[Role] ([ID_Role], [TenRole]) VALUES (3, N'Customer')
GO
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (1, 5, N'Sầu Riêng', N'/Images/38c620ea-9a99-4454-bbd1-fe8e8e7c7bec_cac-loai-sau-rieng-o-singapore-61.png', N'Ngon Lăm nha', 50000, 69, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (6, 5, N'Ổi nữ hoàng', N'/Images/7d369796-a8f8-4e02-a7cd-a7a651e69c48_d237268fe19d60fa8cd3bbd373ab6e21.jpg', N'Rất ngon', 20000, 98, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (9, 5, N'Mít', N'/Images/ec328d7f-f5a7-481c-921c-469e6a496306_loi-ich-cua-qua-mit.jpg', N'Ngon bá cháy', 30000, 143, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (10, 5, N'Dưa hấu', N'/Images/192cd2a2-8b01-4977-9fe9-87f5b8ca41dd_San-pham-dua-hau-fruits-tt.jpg', N'Rất ngon', 15000, 150, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (12, 5, N'Cherry', N'/Images/c5510ceb-5b6e-4a02-adc7-bb521c9d8162_4ab375acf2a0279ddf09e3bc5baa8705.jpg', N'Rất ngon', 567000, 100, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (13, 5, N'Xoài chín', N'/Images/b8641728-fc15-4bb6-9420-6daa95e12f3a_images.jpg', N'Xoài rất chín', 10000, 100, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (14, 5, N'Mận Đỏ', N'/Images/26a50cd0-3844-474b-ab13-bfc398db4b11_man-an-phuoc-tui-500g-202012171420096493.jpg', N'Mận chín 1 đóng', 15000, 1999, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (15, 5, N'Bưởi da xanh', N'/Images/dfae08cc-4274-436f-919a-f0f06e390405_BUOI-DA-XANH.png', N'Bưởi tốt', 50000, 399, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (16, 5, N'Cam xoàn', N'/Images/ff22185b-5bae-45f4-b48d-f9be9b91827e_cong-dung-qua-cam.jpg', N'Cam ngọt nước', 30000, 2000, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (17, 5, N'Lựu', N'/Images/6ed0a66f-4fd9-425e-956f-bed4424ee436_pomegranate_image_hrws.png', N'Lựu rất đỏ', 45000, 100, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (18, 5, N'Chôm chôm', N'/Images/0091dc3b-bd1c-447b-9cc4-af2cae3a05cd_20210319_064535_531791_an-chom-chom.max-800x800.jpg', N'Chôm chôm ngon bá cháy', 35000, 2999, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (19, 5, N'Dưa chuột có sừng', N'/Images/d7b9dac1-e43c-4bc8-935d-50634c39338c_4.jpg', N'Dưa chuột siêu ngon khiêu người ăn gục ngã', 1500000, 200, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (20, 5, N'Măng cụt', N'/Images/4753752d-f592-49e5-8445-33e73d3b16c7_20210408_091809_242710_dinh-duong-cua-mang.max-1800x1800.jpg', N'Ngon', 50000, 0, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (21, 5, N'Trái theobroma Grandiflorum', N'/Images/78c03f5d-b5e4-43c9-b575-86ee3552cd59_10-loai-trai-cay-ngon-nhat-the-gioi-trong-do-co-3-loai-ma-nguoi-viet-nao-cung-tung-an-roi-202007281159522969.jpg', N'Trái lạ lẫm', 2000000, 0, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (22, 12, N'Vú sữa hoàng kim', N'/Images/d5866db2-9860-41d5-8f51-86bd782d0171_vu-sua-hoang-kim-4-1024x578.jpg', N'Rất ngon', 50000, 399, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (23, 12, N'Nhãn', N'/Images/f17369db-731a-4628-9bfb-546be426df3b_qua-nhan-01.jpg', N'Ngon Qúa', 70000, 2000, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (24, 12, N'Thanh long ruột đỏ', N'/Images/0eb64a4f-c721-4458-b46c-c152131caf7c_1.jpg', N'Đỏ tươi', 22000, 1234, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (25, 12, N'Vãi thiều', N'/Images/0d282a06-e1ea-4a88-a173-1513b21a85d2_vai-thieu-tuoi-1.png', N'Ngon', 45000, 2999, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (26, 12, N'Đào Sa Pa', N'/Images/44e09f1d-d058-4d60-a31e-348fca3696c5_dao-sapa-lao-cai.jpg', N'ngon', 25000, 998, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (27, 12, N'Lê', N'/Images/f901a73c-97c0-44f1-a075-564217368c5f_le-duong-tui-1kg-4-6-trai-202203100757479793.jpg', N'Ngon', 45000, 895, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (29, 12, N'Nho', N'/Images/6ec6ff3d-7d5c-4057-a13f-2ce3efd95723_1nho_den_6677263_26112019.jpg', N'Rất ngon', 30000, 989, 2)
INSERT [dbo].[SanPham] ([ID_SanPham], [ID_TaiKhoan], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [GiaSanPham(VND)], [SoLuongSanPham], [ID_TrangThaiSanPham]) VALUES (30, 12, N'Bơ', N'/Images/51480ce7-0dcc-425f-9e61-db93b56d4cdd_20a.jpg', N'Rất ngon', 30000, 100, 2)
SET IDENTITY_INSERT [dbo].[SanPham] OFF
GO
INSERT [dbo].[SanPhamTrongGioHang] ([ID_TaiKhoan], [ID_SanPham], [SoLuong(KG)]) VALUES (4, 1, 1)
INSERT [dbo].[SanPhamTrongGioHang] ([ID_TaiKhoan], [ID_SanPham], [SoLuong(KG)]) VALUES (6, 18, 10)
INSERT [dbo].[SanPhamTrongGioHang] ([ID_TaiKhoan], [ID_SanPham], [SoLuong(KG)]) VALUES (14, 25, 10)
GO
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'ok', CAST(N'2022-03-18T13:09:10.940' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'dc', CAST(N'2022-03-21T13:32:39.070' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'dc', CAST(N'2022-03-21T15:20:36.350' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'dc', CAST(N'2022-03-21T15:36:40.490' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'"B?n dã dang kí tài kho?n thành công, chúc b?n mua s?m t?i shop vui v? ?"', CAST(N'2022-03-21T15:38:44.710' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-21T15:42:20.927' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-21T15:44:03.677' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-21T16:14:41.083' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-21T16:27:39.540' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Admin không duyệt cho bạn trở thành người hàng, vui lòng kiểm tra lại thông tin tài khoản có phù hợp không.', CAST(N'2022-03-21T16:28:02.690' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (7, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-21T16:28:40.983' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (7, N'Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.', CAST(N'2022-03-21T16:28:59.707' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T19:50:53.067' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 9 của bản đã được xác nhận', CAST(N'2022-03-21T19:51:20.810' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 8 của bạn đã bị từ chối', CAST(N'2022-03-21T19:51:24.913' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T19:54:01.320' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 10 đã bị khách hàng hủy', CAST(N'2022-03-21T19:54:10.880' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T19:57:53.800' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 11 của bản đã được xác nhận', CAST(N'2022-03-21T19:58:07.820' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 11 đã giao hàng thành công', CAST(N'2022-03-21T19:58:32.830' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T20:50:28.203' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 12 của bản đã được xác nhận', CAST(N'2022-03-21T20:50:50.347' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 12 đã giao hàng thành công', CAST(N'2022-03-21T20:51:10.183' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 1500 VND', CAST(N'2022-03-21T20:51:10.187' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-21T21:37:37.103' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Có người dùng đăng kí thành người bán hàng!', CAST(N'2022-03-21T21:37:37.107' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.', CAST(N'2022-03-21T21:37:56.733' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Admin không duyệt cho bạn trở thành người hàng, vui lòng kiểm tra lại thông tin tài khoản có phù hợp không.', CAST(N'2022-03-21T21:37:57.970' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T22:59:05.560' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T23:02:36.810' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 15 đã bị khách hàng hủy', CAST(N'2022-03-21T23:02:45.977' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 14 đã bị khách hàng hủy', CAST(N'2022-03-21T23:02:47.917' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 13 đã bị khách hàng hủy', CAST(N'2022-03-21T23:02:49.937' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T23:03:55.860' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T11:48:35.413' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T12:34:05.620' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T12:34:05.623' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Sầu Riêng thành công', CAST(N'2022-03-22T12:36:28.560' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Dưa Hấu thành công', CAST(N'2022-03-22T12:37:31.170' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T12:38:05.797' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T12:38:05.800' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T12:40:21.360' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T12:40:21.363' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Dưa Hấu đã bị từ chối', CAST(N'2022-03-22T12:53:13.540' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T13:08:00.303' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T14:24:34.283' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:06:27.850' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Ổi nữ hoàng thành công', CAST(N'2022-03-22T20:06:54.903' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-22T20:49:26.157' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-22T20:49:28.173' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Mít thành công', CAST(N'2022-03-22T20:49:29.180' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Dưa hấu thành công', CAST(N'2022-03-22T20:49:29.817' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Táo thành công', CAST(N'2022-03-22T20:49:30.450' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Cherry thành công', CAST(N'2022-03-22T20:49:31.943' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:20:49.553' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-22T21:24:34.480' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:05:24.140' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:05:24.143' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:17:08.080' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:17:08.080' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:17:21.340' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:17:21.340' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:17:47.217' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:17:47.217' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:18:01.527' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:18:01.527' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (11, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-21T15:47:49.400' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-21T21:20:05.803' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Sơn thành công', CAST(N'2022-03-21T21:20:32.547' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-21T21:20:54.793' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (11, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-21T21:27:43.503' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (11, N'Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.', CAST(N'2022-03-21T21:28:07.563' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-21T21:28:27.147' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (11, N'Đăng kí bán sản phẩm 1 thành công', CAST(N'2022-03-21T21:28:44.883' AS DateTime), 0)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 thành công', CAST(N'2022-03-21T21:28:45.370' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-21T21:32:06.260' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T23:30:02.047' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 18 đã bị khách hàng hủy', CAST(N'2022-03-21T23:30:08.160' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Dưa Hấu đã bị từ chối', CAST(N'2022-03-22T12:43:21.080' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Sầu Riêng thành công', CAST(N'2022-03-22T12:43:24.083' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T12:45:18.263' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T12:45:18.267' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Sầu Riêng thành công', CAST(N'2022-03-22T12:45:36.390' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T13:43:41.927' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:42:42.547' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:43:05.370' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:44:12.153' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:03:47.137' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:04:46.873' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:05:19.550' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:05:59.590' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:06:48.143' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:10:34.460' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:11:35.560' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:12:17.283' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Xoài chín thành công', CAST(N'2022-03-22T21:12:36.640' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Mận Đỏ thành công', CAST(N'2022-03-22T21:12:37.417' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Bưởi da xanh thành công', CAST(N'2022-03-22T21:12:37.957' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Cam xoàn thành công', CAST(N'2022-03-22T21:12:38.430' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Lựu thành công', CAST(N'2022-03-22T21:12:38.860' AS DateTime), 1)
GO
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Chôm chôm thành công', CAST(N'2022-03-22T21:12:39.373' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Dưa chuột có sừng thành công', CAST(N'2022-03-22T21:12:39.807' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Măng cụt đã bị từ chối', CAST(N'2022-03-22T21:12:40.237' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:16:24.650' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:16:24.650' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:16:56.280' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:16:56.280' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:18:07.227' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:18:07.230' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:19:05.303' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:19:05.307' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-22T21:32:37.607' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Có người dùng đăng kí thành người bán hàng!', CAST(N'2022-03-22T21:32:37.607' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.', CAST(N'2022-03-22T21:33:05.353' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:34:28.870' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:35:05.813' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:35:51.760' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:38:43.477' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T21:39:38.023' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:57:14.700' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:57:14.700' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:57:40.060' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:57:40.060' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:58:02.650' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:58:02.650' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:58:21.250' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:58:21.250' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:58:42.487' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:58:42.487' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:59:37.407' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:59:37.410' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:59:57.560' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T23:00:52.590' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T23:03:03.270' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-21T23:30:31.720' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 19 đã bị khách hàng hủy', CAST(N'2022-03-21T23:30:41.833' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T12:52:50.153' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T12:52:50.163' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:35:29.637' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:35:48.183' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T20:37:08.880' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:59:57.563' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:00:18.740' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:00:18.740' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:00:34.473' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:00:34.477' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:00:52.147' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:00:52.150' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:01:10.587' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:01:10.587' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:01:30.300' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:01:30.303' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:01:46.627' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:01:46.627' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:02:25.027' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:02:25.030' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:02:39.623' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:02:39.627' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-22T22:16:32.387' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-22T22:16:42.707' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T22:27:17.040' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 26 đã giao hàng thành công', CAST(N'2022-03-22T23:04:50.230' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 11550 VND', CAST(N'2022-03-22T23:04:50.230' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 25 đã giao hàng thành công', CAST(N'2022-03-22T23:04:51.847' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 20100 VND', CAST(N'2022-03-22T23:04:51.850' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T23:18:32.040' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 27 đã bị khách hàng hủy', CAST(N'2022-03-22T23:18:43.320' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 28 đã bị khách hàng hủy', CAST(N'2022-03-22T23:18:48.227' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T23:18:58.130' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T21:14:22.463' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T21:14:22.467' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:07:51.690' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:07:51.693' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T23:19:19.743' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-22T22:18:33.633' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-22T22:18:33.633' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-22T22:19:22.313' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T23:03:53.740' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T23:03:53.753' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 25 của bản đã được xác nhận', CAST(N'2022-03-22T23:04:20.327' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 24 của bản đã được xác nhận', CAST(N'2022-03-22T23:04:23.623' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 26 của bản đã được xác nhận', CAST(N'2022-03-22T23:04:37.250' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-22T23:18:21.620' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (13, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-23T13:22:01.893' AS DateTime), 0)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T11:19:01.890' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T14:31:29.457' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 31 đã bị khách hàng hủy', CAST(N'2022-03-24T14:31:57.547' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T21:36:23.167' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 33 của bản đã được xác nhận', CAST(N'2022-03-24T21:36:38.853' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 33 đã giao hàng thành công', CAST(N'2022-03-24T21:36:46.827' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 1500 VND', CAST(N'2022-03-24T21:36:46.830' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T21:37:23.240' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T21:37:44.153' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 35 của bản đã được xác nhận', CAST(N'2022-03-24T21:37:54.083' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 35 đã giao hàng thành công', CAST(N'2022-03-24T21:38:01.380' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 1050 VND', CAST(N'2022-03-24T21:38:01.383' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 38 của bạn đã bị từ chối', CAST(N'2022-03-25T09:57:52.690' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 36 của bản đã được xác nhận', CAST(N'2022-03-25T09:58:18.387' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 44 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:05.187' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 43 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:07.353' AS DateTime), 1)
GO
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 42 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:08.870' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 41 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:11.340' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 24 đã giao hàng thành công', CAST(N'2022-03-24T08:35:24.077' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 600 VND', CAST(N'2022-03-24T08:35:24.083' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:51:58.020' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:51:58.027' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:55:29.773' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:55:58.703' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:56:12.390' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:56:32.713' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 40 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:12.883' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 39 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:14.483' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 30 đã bị khách hàng hủy', CAST(N'2022-03-25T10:58:15.900' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T10:58:54.480' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-25T11:08:10.120' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 45 đã bị khách hàng hủy', CAST(N'2022-03-25T11:08:35.243' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 29 của bản đã được xác nhận', CAST(N'2022-03-24T10:10:54.597' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-24T11:50:56.463' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T21:52:03.180' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T21:52:03.190' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-24T21:52:15.000' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-24T21:54:16.307' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 thành công', CAST(N'2022-03-24T21:54:41.667' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-25T11:25:18.823' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-25T11:26:27.363' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-25T11:26:41.983' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-25T11:27:26.850' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-25T11:27:27.650' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đăng kí bán sản phẩm Nho thành công', CAST(N'2022-03-25T11:27:30.147' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 46 đã giao hàng thành công', CAST(N'2022-03-25T11:28:04.673' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 114750 VND', CAST(N'2022-03-25T11:28:04.677' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-25T11:29:59.743' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-25T11:30:53.740' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Có người dùng đăng kí thành người bán hàng!', CAST(N'2022-03-25T11:30:53.743' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Admin không duyệt cho bạn trở thành người hàng, vui lòng kiểm tra lại thông tin tài khoản có phù hợp không.', CAST(N'2022-03-25T11:31:11.673' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-25T11:32:29.273' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-25T11:32:29.277' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đăng kí bán sản phẩm Nhãn thành công', CAST(N'2022-03-25T11:33:05.330' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-25T11:34:53.960' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-25T11:34:53.960' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Trái theobroma Grandiflorum đã bị từ chối', CAST(N'2022-03-25T11:35:18.280' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (14, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-25T11:38:40.020' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (15, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-25T21:14:59.787' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-25T21:23:08.810' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 36 đã giao hàng thành công', CAST(N'2022-03-25T21:24:07.227' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 900 VND', CAST(N'2022-03-25T21:24:07.230' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 34 đã giao hàng thành công', CAST(N'2022-03-25T21:24:14.610' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 750 VND', CAST(N'2022-03-25T21:24:14.613' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 29 đã giao hàng thành công', CAST(N'2022-03-25T11:11:08.777' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 1500 VND', CAST(N'2022-03-25T11:11:08.777' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 32 của bạn đã bị từ chối', CAST(N'2022-03-25T11:20:56.090' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 37 của bạn đã bị từ chối', CAST(N'2022-03-25T11:21:47.323' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (6, N'Đơn hàng 46 của bản đã được xác nhận', CAST(N'2022-03-25T11:21:59.600' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 34 của bản đã được xác nhận', CAST(N'2022-03-25T11:22:01.493' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-25T21:50:54.010' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-25T21:51:01.860' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-25T21:51:20.823' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-25T21:51:21.780' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T07:30:21.130' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T07:32:32.433' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T07:34:53.927' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T07:35:01.950' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T07:35:49.400' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T07:36:05.703' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T08:31:13.587' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (16, N'Đơn hàng 55 của bạn đã bị từ chối', CAST(N'2022-03-26T08:32:00.480' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T08:33:23.303' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T08:33:23.323' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (16, N'Đơn hàng 56 của bản đã được xác nhận', CAST(N'2022-03-26T08:33:50.690' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 57 đã bị khách hàng hủy', CAST(N'2022-03-26T08:35:25.920' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đơn hàng 56 đã giao hàng thành công', CAST(N'2022-03-26T08:36:06.023' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 1500 VND', CAST(N'2022-03-26T08:36:06.027' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đăng kí bán sản phẩm Bơ thành công', CAST(N'2022-03-26T10:04:50.440' AS DateTime), 0)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-26T10:05:38.307' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm 1 đã bị từ chối', CAST(N'2022-03-26T10:05:55.697' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (16, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-26T08:10:48.643' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (16, N'Bạn đã nạp tiền vào tài khoản thành công', CAST(N'2022-03-26T08:12:35.920' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T08:12:45.763' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T08:15:12.113' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (16, N'Đơn hàng 54 của bản đã được xác nhận', CAST(N'2022-03-26T08:16:18.170' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (16, N'Đơn hàng 53 của bạn đã bị từ chối', CAST(N'2022-03-26T08:16:26.420' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một Sản phẩm cần xác nhận đăng bán!', CAST(N'2022-03-26T08:17:20.160' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Đơn hàng 54 đã giao hàng thành công', CAST(N'2022-03-26T08:19:36.320' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một đơn hàng giao thành công bạn nhận được 14850 VND', CAST(N'2022-03-26T08:19:36.323' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (17, N'Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥', CAST(N'2022-03-26T10:10:34.620' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (17, N'Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!', CAST(N'2022-03-26T10:12:39.130' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Có người dùng đăng kí thành người bán hàng!', CAST(N'2022-03-26T10:12:39.133' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (17, N'Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.', CAST(N'2022-03-26T10:13:03.480' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (17, N'Admin không duyệt cho bạn trở thành người hàng, vui lòng kiểm tra lại thông tin tài khoản có phù hợp không.', CAST(N'2022-03-26T10:13:31.520' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!', CAST(N'2022-03-26T10:14:45.560' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (4, N'Một sản phẩm đang chò xác nhận!', CAST(N'2022-03-26T10:14:45.563' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Đăng kí bán sản phẩm Sầu Riêng thành công', CAST(N'2022-03-26T10:15:13.450' AS DateTime), 1)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (5, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T10:16:11.770' AS DateTime), 0)
INSERT [dbo].[ThongBao] ([ID_TaiKhoan], [NoiDung], [NgayThongBao], [isWatch]) VALUES (12, N'Bạn có đơn hàng đang chờ xác nhận', CAST(N'2022-03-26T10:16:11.780' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[ThongTinTaiKhoan] ON 

INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (4, N'admin123', N'25d55ad283aa400af464c76d713c07ad', N'Admin', 2001, N'0123456789', N'1234', 170550, 0, 0, N'/Images/0030b3f7-1068-4e18-9bdb-7037a2003d6f_ava mac dinh.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (5, N'ngocson1', N'25d55ad283aa400af464c76d713c07ad', N'Ngoc Son', 2001, N'0854703523', N'FPT Cần Thơ', 3840850, 1, 0, N'/Images/e7a93deb-9c29-4268-9b42-10f33fd96438_ava mac dinh.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (6, N'ngocson2', N'c5fde9de2d29789a81d1bc0f16292048', N'Ngọc Sơn', 2002, N'0122222222', N'123', 2084567, 3, 0, N'/Images/e606ed22-ee10-40c6-96e0-00ab178bd536_1nho_den_6677263_26112019.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (7, N'ngocson3', N'25d55ad283aa400af464c76d713c07ad', N'NgocSon', 2001, N'0123456789', N'1', 0, 1, 1, N'/Images/c7c06303-0604-43cf-a00b-708f6fabb367_ava mac dinh.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (11, N'ngocson10', N'25d55ad283aa400af464c76d713c07ad', N'1', 2001, N'0123456789', N'1', 0, 1, 1, N'/Images/d5d41a50-c991-42d0-b424-ef3d0921ceff_avatarmacdinh.png')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (12, N'koreashop123', N'25d55ad283aa400af464c76d713c07ad', N'Korea Fruits', 2001, N'0123456789', N'1', 4788100, 1, 0, N'/Images/b460ca39-0596-4b48-8700-c96830583349_avatar-trang-den-cuc-chat-lu.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (13, N'duyennp150', N'25d55ad283aa400af464c76d713c07ad', N'Duyen', 2001, N'0854703523', N'1', 0, 3, 1, N'/Images/ebaa135b-c8c1-4a5a-b420-96a6a05db60f_avatar-trang-den-cuc-chat-lu.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (14, N'ngocson9', N'25d55ad283aa400af464c76d713c07ad', N'FPT', 0, N'0', N'0', 0, 3, 1, N'/Images/ebaa135b-c8c1-4a5a-b420-96a6a05db60f_avatar-trang-den-cuc-chat-lu.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (15, N'thachbui', N'25d55ad283aa400af464c76d713c07ad', N'thach bui', 2001, N'0854703523', N'2', 0, 3, 0, N'/Images/0030b3f7-1068-4e18-9bdb-7037a2003d6f_ava mac dinh.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (16, N'dinhvinh', N'25f9e794323b453885f5181f1b624d0b', N'Đình Vinh', 2001, N'0854703523', N'FPT Cần Thơ', 10000, 3, 0, N'/Images/0030b3f7-1068-4e18-9bdb-7037a2003d6f_ava mac dinh.jpg')
INSERT [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [Tien], [ID_Role], [isDelete], [Avt]) VALUES (17, N'hoaiphong', N'25d55ad283aa400af464c76d713c07ad', N'Hoài Phong', 2001, N'0854703523', N'Cần Thơ', 0, 3, 0, N'/Images/0030b3f7-1068-4e18-9bdb-7037a2003d6f_ava mac dinh.jpg')
SET IDENTITY_INSERT [dbo].[ThongTinTaiKhoan] OFF
GO
INSERT [dbo].[TrangThaiDonHang] ([ID_TrangThaiDonHang], [TenTrangThaiDonHang]) VALUES (1, N'Chưa Xác Nhận')
INSERT [dbo].[TrangThaiDonHang] ([ID_TrangThaiDonHang], [TenTrangThaiDonHang]) VALUES (2, N'Đã Xác Nhận')
INSERT [dbo].[TrangThaiDonHang] ([ID_TrangThaiDonHang], [TenTrangThaiDonHang]) VALUES (3, N'Đã Nhận')
INSERT [dbo].[TrangThaiDonHang] ([ID_TrangThaiDonHang], [TenTrangThaiDonHang]) VALUES (4, N'Đã Huỷ')
GO
INSERT [dbo].[TrangThaiSanPham] ([ID_TrangThaiSanPham], [Ten_TrangThaiSanPham]) VALUES (1, N'Đăng Ký Bán')
INSERT [dbo].[TrangThaiSanPham] ([ID_TrangThaiSanPham], [Ten_TrangThaiSanPham]) VALUES (2, N'Đã Đăng Ký')
INSERT [dbo].[TrangThaiSanPham] ([ID_TrangThaiSanPham], [Ten_TrangThaiSanPham]) VALUES (3, N'Đã Xoá')
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_HoaDon] FOREIGN KEY([ID_HoaDon])
REFERENCES [dbo].[HoaDon] ([ID_HoaDon])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_HoaDon]
GO
ALTER TABLE [dbo].[DanhGia]  WITH CHECK ADD  CONSTRAINT [FK_DanhGia_SanPham] FOREIGN KEY([ID_SanPham])
REFERENCES [dbo].[SanPham] ([ID_SanPham])
GO
ALTER TABLE [dbo].[DanhGia] CHECK CONSTRAINT [FK_DanhGia_SanPham]
GO
ALTER TABLE [dbo].[DanhGia]  WITH CHECK ADD  CONSTRAINT [FK_DanhGia_ThongTinTaiKhoan1] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[DanhGia] CHECK CONSTRAINT [FK_DanhGia_ThongTinTaiKhoan1]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_PhuongThucThanhToan] FOREIGN KEY([ID_PhuongThucThanhToan])
REFERENCES [dbo].[PhuongThucThanhToan] ([ID_PhuongThucThanhToan])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_PhuongThucThanhToan]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_TaiKhoan] FOREIGN KEY([ID_TaiKhoanCus])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_TaiKhoan]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_TaiKhoan1] FOREIGN KEY([ID_TaiKhoanSale])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_TaiKhoan1]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_TrangThaiDonHang] FOREIGN KEY([ID_TrangThaiDonHang])
REFERENCES [dbo].[TrangThaiDonHang] ([ID_TrangThaiDonHang])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_TrangThaiDonHang]
GO
ALTER TABLE [dbo].[LichSuTimKiem]  WITH CHECK ADD  CONSTRAINT [FK_LichSuTimKiem_ThongTinTaiKhoan] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[LichSuTimKiem] CHECK CONSTRAINT [FK_LichSuTimKiem_ThongTinTaiKhoan]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_TaiKhoan] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_TaiKhoan]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_TrangThaiSanPham] FOREIGN KEY([ID_TrangThaiSanPham])
REFERENCES [dbo].[TrangThaiSanPham] ([ID_TrangThaiSanPham])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_TrangThaiSanPham]
GO
ALTER TABLE [dbo].[SanPhamTrongGioHang]  WITH CHECK ADD  CONSTRAINT [FK_SanPhamTrongGioHang_ThongTinTaiKhoan] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[SanPhamTrongGioHang] CHECK CONSTRAINT [FK_SanPhamTrongGioHang_ThongTinTaiKhoan]
GO
ALTER TABLE [dbo].[ThongBao]  WITH CHECK ADD  CONSTRAINT [FK_ThongBao_ThongTinTaiKhoan] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinTaiKhoan] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[ThongBao] CHECK CONSTRAINT [FK_ThongBao_ThongTinTaiKhoan]
GO
ALTER TABLE [dbo].[ThongTinTaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_ThongTinTaiKhoan_Role] FOREIGN KEY([ID_Role])
REFERENCES [dbo].[Role] ([ID_Role])
GO
ALTER TABLE [dbo].[ThongTinTaiKhoan] CHECK CONSTRAINT [FK_ThongTinTaiKhoan_Role]
GO
/****** Object:  StoredProcedure [dbo].[CHECKID]    Script Date: 3/26/2022 10:21:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CHECKID] @checkid_tk int, @checkid_sp int, @soluong int
AS
Declare @ChecksoluongKHO int, @ChecksoluongTHEM int;
BEGIN
set @ChecksoluongKHO = (select SoLuongSanPham from SanPham where ID_SanPham = @checkid_sp);
set @ChecksoluongTHEM = (select [SoLuong(KG)] from SanPhamTrongGioHang where ID_TaiKhoan = @checkid_tk and ID_SanPham = @checkid_sp);
 if exists (select * from SanPhamTrongGioHang where ID_TaiKhoan = @checkid_tk and ID_SanPham = @checkid_sp)
			BEGIN
					if(@ChecksoluongTHEM + @soluong > @ChecksoluongKHO)
					UPDATE SanPhamTrongGioHang set [SoLuong(KG)] = @ChecksoluongKHO where ID_TaiKhoan = @checkid_tk and ID_SanPham = @checkid_sp
					else
					UPDATE SanPhamTrongGioHang set [SoLuong(KG)] = [SoLuong(KG)] + @soluong where ID_TaiKhoan = @checkid_tk and ID_SanPham = @checkid_sp
			END
else
			BEGIN
					INSERT INTO SanPhamTrongGioHang Values(@checkid_tk, @checkid_sp,@soluong)
			END
END			
GO
USE [master]
GO
ALTER DATABASE [Fruits_World] SET  READ_WRITE 
GO
