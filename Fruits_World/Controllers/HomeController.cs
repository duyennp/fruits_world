﻿using Fruits_World.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
namespace Fruits_World.Controllers
{
    public class HomeController : Controller
    {
        ProductDAO ProductDAO = new ProductDAO();
        public IActionResult Index()
        {
            string id = HttpContext.Session.GetString("id");
            if (id == null)
            {
                HttpContext.Session.SetString("id", "0");
            }
            ViewBag.list10Fruits = ProductDAO.getProductforIndex();
            ViewBag.listhighlights = ProductDAO.getHighlightsProduct();
            return View();
        }

        public IActionResult Privacy()
        {

            return View();
        }
        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}