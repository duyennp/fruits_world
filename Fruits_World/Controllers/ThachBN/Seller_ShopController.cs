﻿using Fruits_World.Models.ThachBN;
using Fruits_World.Models.ThachBN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;
namespace Fruits_World.Controllers.ThachBN
{
    public class Seller_ShopController : Controller
    {
        ProductDAO dao = new ProductDAO();
        AccountDAO AccountDAO = new AccountDAO();
        Models.DuyenPD.DAO.ProductDAO proDAO = new Models.DuyenPD.DAO.ProductDAO();
        Models.DuyenPD.DAO.AccountDAO accDAO = new Models.DuyenPD.DAO.AccountDAO();

        //shop va thong tin san pham trong shop
        public IActionResult Seller_Shop(string id, int? sortPrice)
        {
            Account account = AccountDAO.getAccountbyID(id);
            List<Product> listPro = null;
            if (sortPrice == null)
            {
                 listPro = dao.getAllProductofShop(account.id_Account);
                 ViewBag.sortPrice = 0;
            }
            else if(sortPrice == 1)
            {
                listPro = dao.getAllProductofShopSortPrice(account.id_Account, "");
                ViewBag.sortPrice = 0;
            }
            else
            {
                listPro = dao.getAllProductofShopSortPrice(account.id_Account, "desc");
                ViewBag.sortPrice = 1;
            }
            
            int page = 1;
            int quantity = 20;

            int endPage = listPro.Count / quantity; // tổng số trang
            if (listPro.Count % quantity != 0)
            {
                endPage++;// nếu thiếu trang thì cộng 1
            }
            var listProPage = proDAO.GetListProductPaging(listPro, page, quantity);

            ViewBag.endPage = endPage;
            ViewBag.Page = page;

            ViewBag.listproduct = listProPage;
            string trungBinhSao = accDAO.GetAverageStarsByID(id);
            if (trungBinhSao == null)
            {
                trungBinhSao = "5";
            } 
            ViewBag.TrungBinhSao = trungBinhSao;
            return View(account);
        }
    }
}
