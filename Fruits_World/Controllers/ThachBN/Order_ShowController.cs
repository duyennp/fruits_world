﻿using Fruits_World.Models.ThachBN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.ThachBN
{
    public class Order_ShowController : Controller
    {
        ProductDAO dao = new ProductDAO();
        Models.SonNN.DAO.OrderDAO orderDAO = new Models.SonNN.DAO.OrderDAO();
        Models.SonNN.DAO.AccountDAO accountDAO = new Models.SonNN.DAO.AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //show order
        public IActionResult Order_Confirmed()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }
            return View();
        }

        //update status order
        public IActionResult Order_confirmReceivedProduct(string id, string idhoadon, string status)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            dao.updateOrder(idhoadon, status);
            Account Adminaccount = accountDAO.getAccountbyUsername("admin123");
            int price = orderDAO.getPriceofOrder(idhoadon);
            int amountforAdmin = price * 3 / 100;
            price = price - amountforAdmin;
            accountDAO.updateMoney(id, price);
            accountDAO.updateMoney(Adminaccount.id_Account, amountforAdmin);
            Order order = orderDAO.getOrderbyID(idhoadon);
            accountDAO.InsertNotification(order.id_Seller, "Đơn hàng " + idhoadon + " đã giao hàng thành công");
            accountDAO.InsertNotification(Adminaccount.id_Account, "Một đơn hàng giao thành công bạn nhận được " + amountforAdmin + " VND");
            return RedirectToAction("Order_Confirmed");
        }

        //order 
        public IActionResult Order_ReceivedProduct()
        {
            return View();

        }

        //order đã huỷ
        public IActionResult Order_Cancelled()
        {
            return View();
        }


    }
}
