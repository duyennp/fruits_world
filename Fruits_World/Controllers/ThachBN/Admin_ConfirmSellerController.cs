﻿using Fruits_World.Models.ThachBN.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.ThachBN
{
    public class Admin_ConfirmSellerController : Controller
    {
        AccountDAO dao = new AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //danh sach dang ky sales
        public IActionResult admin_listRegistedtoSeller()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            ViewBag.Seller = dao.getAllAccountRegistedtoSeller();
            return View();
        }

        //update role 
        public IActionResult updateRole(string? id, int role)
        {
            int? role2 = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role2))
            {
                return NotFound();
            }

            dao.updateRole(id, role);
            return RedirectToAction("admin_listRegistedtoSeller", "Admin_ConfirmSeller");
        }
    }
}
