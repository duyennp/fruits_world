﻿using Fruits_World.Models.ThachBN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.ThachBN
{
    public class Admin_StatisticsController : Controller
    {
        AccountDAO dao = new AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //danh sach acc mua nhieu nhat
        public IActionResult Admin_ListAccountBuyMostAtShop()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            var list = dao.getAllAccountBuyMostAtWeb();
            return View(list);
        }

        //thống kê shop được danh gia chat luong nhat
        public IActionResult BestSallerStars()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            List<Account> listAcc = dao.GetSallerBestStars();
            if (listAcc.Count > 0)
            {
                ViewBag.Acc = listAcc;
            }

            return View();
        }
    }
}
