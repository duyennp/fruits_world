﻿using Fruits_World.Models.ThachBN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.ThachBN
{
    public class Admin_ConfirmProductController : Controller
    {
        ProductDAO dao = new ProductDAO();
        Models.SonNN.DAO.AccountDAO accountDAO = new Models.SonNN.DAO.AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //danh sach dang ky ban hang
        public IActionResult admin_listRegistedProduct()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            ViewBag.listFruits = dao.getAllProductbyStatus(1);
            return View();
        }

        //update trang thai cho cus thanh nguoi ban
        public IActionResult updateStatus(string? id, int status)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            Product product = dao.getProductbyId(id);
            if(status == 2)
            {
                accountDAO.InsertNotification(product.id_Account, "Đăng kí bán sản phẩm " + product.productName + " thành công");

            }
            else
            {
                accountDAO.InsertNotification(product.id_Account, "Đăng kí bán sản phẩm " + product.productName + " đã bị từ chối");
                dao.deleteProduct(id);
            }

            dao.updateStatusFruit(id, status);
            return RedirectToAction("admin_listRegistedProduct", "Admin_ConfirmProduct");
        }
    }
}
