﻿using Fruits_World.Models;
using Fruits_World.Models.DuyenPD.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class InfoProductController : Controller
    {
        BillDAO billDAO = new BillDAO();
        DecentralizationDAO phanquyen =   new DecentralizationDAO();

        //controller lấy thông tin danh sách sản phẩm qua id order
        public IActionResult InfoProductByIDBill(string? idBill)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            if (string.IsNullOrEmpty(idBill))
            {
                return NotFound();
            }
            List<OrderDetails> list = billDAO.ListProductByIDBill(idBill);
            return View(list);
        }
    }
}
