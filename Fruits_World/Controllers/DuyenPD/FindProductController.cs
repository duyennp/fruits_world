﻿using Fruits_World.Models.DuyenPD.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class FindProductController : Controller
    {
        AccountDAO accountDAO = new AccountDAO();
        ProductDAO productDAO = new ProductDAO();

        //controller tim kiếm tên shop or tên sản phẩm theo name
        public IActionResult Search(string? searchContent, int? option, int page = 1)
        {
            if (searchContent != null)
            {
                searchContent = searchContent.Replace("\'", "").Replace("\"", "");
                if (searchContent.Length == 0)
                {
                    searchContent = null;
                }
            }

            var listPro = new List<Product>();
            var listProSoldMost = productDAO.GetListProductSoldMost(null);
            int min = 0, max = 1; // min < price < max
            if (option != null) // có sự lựa chọn option
            {
                switch (option)
                {
                    case 1: min = 0; max = 100000; break;
                    case 2: min = 100000; max = 200000; break;
                    case 3: min = 200000; max = 500000; break;
                    case 4: min = 500000; max = 0; break; // max = 0 là khong gioi han
                }

                if (searchContent != null) // có option và nội dung search
                {
                    if (max != 0) // price có max
                    {
                        foreach (var item in listProSoldMost)
                        {
                            if (item.productName.ToLower().Contains(searchContent.ToLower()) && item.productPrice >= min && item.productPrice <= max)
                            {
                                listPro.Add(item);
                            }
                        }
                    }
                    else // price ko co max chỉ có min
                    {
                        foreach (var item in listProSoldMost)
                        {
                            if (item.productName.ToLower().Contains(searchContent.ToLower()) && item.productPrice >= min)
                            {
                                listPro.Add(item);
                            }
                        }
                    }
                }
                else // ko có search
                {
                    if (max != 0) // có price max
                    {
                        foreach (var item in listProSoldMost)
                        {
                            if (item.productPrice >= min && item.productPrice <= max)
                            {
                                listPro.Add(item);
                            }
                        }
                    }
                    else // ko có price max
                    {
                        foreach (var item in listProSoldMost)
                        {
                            if (item.productPrice >= min)
                            {
                                listPro.Add(item);
                            }
                        }
                    }
                }
            }
            else // ko có option
            {
                if (searchContent != null) // có search
                {
                    var listAcc = accountDAO.ListAccountSearchByName(searchContent);
                    if (listAcc.Count != 0)
                    {
                        ViewBag.ListAcc = listAcc;
                    }

                    foreach (var item in listProSoldMost)
                    {
                        if (item.productName.ToLower().Contains(searchContent.ToLower()))
                        {
                            listPro.Add(item);
                        }
                    }
                }
                else // ko option và ko search
                {
                    listPro = listProSoldMost;
                }
            }



            // phân trang
            int count = listPro.Count; //lấy tổng số product trong list
            int quantityProductDisphay = 20; // số lượng hiển thị ra 

            int endPage = count / quantityProductDisphay; // tổng số trang
            if (count % quantityProductDisphay != 0)
            {
                endPage++;// nếu thiếu trang thì cộng 1
            }

            List<Product> listProEnd = productDAO.GetListProductPaging(listPro, page, quantityProductDisphay);
            if (listProEnd.Count != 0) 
            ViewBag.ListPro = listProEnd;

            ViewBag.endPage = endPage;
            ViewBag.SearchContent = searchContent;
            ViewBag.Option = option;
            ViewBag.Page = page;
 
            if (searchContent != null || option != null)
            {
                string mess = searchContent != null ? searchContent : "";
                mess += option != null ? "  giá: " + min : null;

                if (max != 0 && max != 1) mess += " - " + max;

                ViewBag.Mess = mess;
            }

            if (listProEnd.Count == 0)
                 ViewBag.nothing = "Không có sản phẩm phù hợp nhấn để quay lại";

            return View();
        }
    }
}
