﻿using Fruits_World.Models.DuyenPD.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class StatisticsOfAdminController : Controller
    {
        BillDAO billDAO = new BillDAO();
        ProductDAO productDAO = new ProductDAO();
        AccountDAO AccountDAO = new AccountDAO();
        DecentralizationDAO phanquyen = new DecentralizationDAO();

        //controller thống kê các đơn hàng
        public IActionResult Bill()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            var listBill = billDAO.ListBill();
            ViewBag.listBill = listBill;
            return View();
        }

        //thống kê sản phẩm được bán nhiều nhất tại web
        public IActionResult ProductSoldMost()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            List<Product> listPro = productDAO.GetListProductSoldMost(DateTime.Today);
            if (listPro.Count > 0)
            {
                ViewBag.product = listPro;
            }
            return View();
        }

        //thống kê shop được bán nhiều nhất tại web
        public IActionResult BestSaller()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            DateTime start = DateTime.Today.AddMonths(-1);
            DateTime end = DateTime.Today;
            List<Account> listAcc = AccountDAO.ListTotalRevenueOfSaller(start, end);
            if (listAcc.Count > 0)
            {
                ViewBag.Acc = listAcc;
            }
            return View();
        }

        //thong ke doanh thu cho admin theo 1 khoang thoi gian
        public IActionResult MonthlyRevenue(int page = 1)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            DateTime theoPage = DateTime.Today.AddYears(1 - page);
            DateTime startDateTime = DateTime.Today.AddYears(-100); //nhan gia tri ngay bat dau
            DateTime endDateTime = DateTime.Today.AddYears(-100); // nhan gia tri ngay ket thuc
            List<List<Account>> listAcc = new List<List<Account>>(); //list saller doanh thu

            for (int i = 12; i >= 1; i--)
            {
                if (i == 12)
                {
                    startDateTime = DateTime.Today.AddYears(-page + 2).AddMonths(-DateTime.Today.Month).AddDays(-DateTime.Today.Day + 1);
                    endDateTime = DateTime.Today.AddYears(-page + 2).AddMonths(-DateTime.Today.Month + 1).AddDays(-DateTime.Today.Day + 1);
                }
                else
                {
                    startDateTime = new DateTime(theoPage.Year, i, 1);
                    endDateTime = new DateTime(theoPage.Year, i + 1, 1);
                }
                listAcc.Add(AccountDAO.ListTotalRevenueOfSaller(startDateTime, endDateTime));
            }
            ViewBag.listAcc = listAcc;
            ViewBag.Year = theoPage.Year;
            ViewBag.Page = page;
            return View();
        }

    }
}
