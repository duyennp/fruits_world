﻿using Fruits_World.Models;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;
using Fruits_World.Models.DuyenPD.DAO;

namespace Fruits_World.Controllers.DuyenPD
{
    public class Admin_EditAccountController : Controller
    {
        AccountDAO dao = new AccountDAO();
        DecentralizationDAO phanquyen = new DecentralizationDAO();

        //controller lấy danh sách toàn bộ account
        public IActionResult admin_EditAccount()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            var list = dao.getAllAccount();
            return View(list);
        }

        //update info acc
        public IActionResult admin_UpdateInformation(string? id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getAccountbyID(id);
            return View(infor);
        }

        //Post: các thông tin đã nhập phù hợp thì chỉnh sửa thông tin account đó
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult admin_UpdateInformation(string? id, Account account)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            if (id != account.id_Account)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                dao.updateInformation(account);
                return RedirectToAction("admin_EditAccount");
            }
            return View(account);
        }

        //controller xoá  account theo id
        public IActionResult deleteAccount(int id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }
            dao.deleteAccount(id);
            return RedirectToAction("admin_EditAccount");
        }


    }
}
