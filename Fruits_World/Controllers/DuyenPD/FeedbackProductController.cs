﻿using Fruits_World.Models.DuyenPD.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class FeedbackProductController : Controller
    {
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        DecentralizationDAO phanquyen = new DecentralizationDAO();

        //lấy thông tin product để cho người dùng đánh giá
        public IActionResult Feedback(string idBill, string idPro)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            if (idBill == null || idPro == null)
            {
                return NotFound();
            }
            ViewBag.IdBill = idBill;
            ViewBag.IdPro = idPro;
            return View();
        }


        //lấy thông tin đánh giá product từ khách hàng
        [HttpPost]
        public IActionResult Feedback(string idBill, string idPro, int starts, string content)
        {
            if (idBill == null || idPro == null || content == null)
            {
                return NotFound();
            }
            if (starts < 1 || starts > 5)
            {
                return NotFound();
            }
            string start = "" + starts;
            string idAcc = HttpContext.Session.GetString("id");
            if (idAcc == null)
            {
                return NotFound();
            }

            feedbackDAO.FeedbackProduct(idAcc, idBill, idPro, start, content);

            return RedirectToAction("Order_ReceivedProduct","Order_Show");
        }
    }
}
