﻿using Fruits_World.Models.DuyenPD.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class Account_updateMoneyController : Controller
    {
        AccountDAO dao = new AccountDAO();
        Fruits_World.Models.SonNN.DAO.AccountDAO accountDAO = new Fruits_World.Models.SonNN.DAO.AccountDAO();
        DecentralizationDAO phanquyen = new DecentralizationDAO();

        //controller update tiền trong tài khoản
        public IActionResult account_updateMoney(string? id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            if (id == null)
            {
                return NotFound();
            }
            
            var infor = dao.getAccountbyID(id);
            return View(infor);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult account_updateMoney(string? id, int money)
        {
            if (ModelState.IsValid)
            {
                dao.updateMoney(id, money);
                accountDAO.InsertNotification(id,"Bạn đã nạp tiền vào tài khoản thành công");
                return RedirectToAction("accountInformation", "Account_Information");

            }
            return View();
        }
    }
}
