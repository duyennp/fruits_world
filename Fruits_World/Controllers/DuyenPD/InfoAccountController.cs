﻿using Fruits_World.Models.DuyenPD.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class InfoAccountController : Controller
    {
        AccountDAO accountDAO = new AccountDAO(); 
        DecentralizationDAO phanquyen = new DecentralizationDAO();

        //controller lấy thông tin 1 account
        public IActionResult InfoAccountByID(string? idAcc)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isAdmin(role))
            {
                return NotFound();
            }

            if (string.IsNullOrEmpty(idAcc))
            {
                return NotFound();
            }
            Account account = accountDAO.getAccountbyID(idAcc);
            return View(account);
        }
    }
}
