﻿using Fruits_World.Models.DuyenPD.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.DuyenPD
{
    public class registedtoSellerController : Controller
    {
        AccountDAO dao = new AccountDAO();
        DecentralizationDAO phanquyen = new DecentralizationDAO();

        //controller đăng ký saller
        public IActionResult registedtoSeller(string? id, int role)
        {
            int? role1 = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role1))
            {
                return NotFound();
            }
            string idacc = HttpContext.Session.GetString("id");
            Account account = dao.getAccountbyID(idacc);
            if(account.sdt.Equals("0") || account.address.Equals("0"))
            {
                TempData["Mess"] = "Bạn cần điền Số Điện Thoại, Địa Chỉ để đăng kí làm Người Bán Hàng!";
                return RedirectToAction("accountInformation", "Account_Information");

            }
            dao.updateRole(id, role);

            return RedirectToAction("accountInformation", "Account_Information");
        }
    }
}
