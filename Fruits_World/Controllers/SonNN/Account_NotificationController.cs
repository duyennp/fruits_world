﻿using Fruits_World.Models.SonNN.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Account_NotificationController : Controller
    {
        AccountDAO accountDAO = new AccountDAO();   
        //Chuyển trạng thái thông báo qua đã xem
        public IActionResult setIsWatch(string id)
        {
            accountDAO.UpdateNotification(id);
            return View();
        }
    }
}
