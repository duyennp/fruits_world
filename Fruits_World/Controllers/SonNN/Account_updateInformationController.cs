﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;
namespace Fruits_World.Controllers.SonNN
{
    public class Account_updateInformationController : Controller
    {
        AccountDAO dao = new AccountDAO();
        private readonly IWebHostEnvironment iwebHostEnvironment;//Lấy môi trường folder để lưu file vào

        public Account_updateInformationController(IWebHostEnvironment webHostEnvironment)
        {
            iwebHostEnvironment = webHostEnvironment;
        }

        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //Get hiện ra thông tin của tài khoản để chỉnh sửa
        public IActionResult account_UpdateInformation(string? id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }
            
            ViewBag.Mess = "Bạn cần điền Địa chỉ và Số Điện Thoại nếu muốn đặt hàng";
            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getAccountbyID(id);
            return View(infor);
        }

        //Post: các thông tin đã nhập phù hợp thì chỉnh sửa thông tin account đó
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult account_UpdateInformation(string? id, Account account)
        {
            if (id != account.id_Account)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                dao.updateInformation(account);
                return RedirectToAction("accountInformation", "Account_Information");
            }
            return View(account);
        }

        //Get Thay đổi avatar
        public IActionResult changeAvatar()
        {
            return View();
        }

        //Post: Thay đổi ảnh đại diện và lưu file hình ảnh đó vào folder của Project /wwwroot/Images
        // chỉ chấp nhận các file đuôi .png và .jpg
        [HttpPost]
        public IActionResult changeAvatar(IFormFile avatar)

        {
            string text = Path.GetExtension(avatar.FileName);
            if (text.ToLower().EndsWith(".png") || text.ToLower().EndsWith(".jpg"))
            {
                string folder = "Images/";
                folder += Guid.NewGuid().ToString() + "_" + avatar.FileName;
                string a = folder;
                HttpContext.Session.SetString("avatar", "/" + a);
                dao.updateAvatar(HttpContext.Session.GetString("id"), "/" + a);
                ViewBag.test = "Bạn đã thay đổi avatar thành công!";
                string serverFolder = Path.Combine(iwebHostEnvironment.WebRootPath, folder);
                avatar.CopyTo(new FileStream(serverFolder, FileMode.Create));
                return View();
            }
            else
            {
                ViewBag.test = "Bạn chỉ có thể chọn file .png hoặc .jpg";
                return View();
            }

        }


    }
}
