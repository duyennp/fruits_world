﻿using Fruits_World.Models;
using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Order_ShowController : Controller
    {
        AccountDAO AccountDAO = new AccountDAO();
        OrderDAO orderDAO = new OrderDAO();

        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();


        //Hiển thị hóa đơn đang chờ xác nhận của tài khoản
        public IActionResult Order_waitforConfirm()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            return View();
        }
        //Hủy các hóa đơn đang chờ xác nhận
        public IActionResult cancelOrder(string id, string status)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            orderDAO.updateOrder(id, status);
            Order order = orderDAO.getOrderbyID(id);
            AccountDAO.InsertNotification(order.id_Seller, "Đơn hàng " + id + " đã bị khách hàng hủy");
            List<OrderDetails> listOrder = orderDAO.getAllOrderDetailsbyIDOrder(id);
            foreach (OrderDetails detail in listOrder)
            {
                orderDAO.updateQuantity(detail.id_Product, detail.buy_Quantity);
            }
            return RedirectToAction("Order_waitforConfirm");
        }
        //Hủy hóa đơn đang chờ xác nhận (chức năng này của seller)
        public IActionResult cancelOrderforSeller(string id, string status)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            orderDAO.updateOrder(id, status);
            Order order = orderDAO.getOrderbyID(id);
            AccountDAO.InsertNotification(order.id_Customer, "Đơn hàng " + id + " của bạn đã bị từ chối");
            List<OrderDetails> listOrder = orderDAO.getAllOrderDetailsbyIDOrder(id);
            foreach (OrderDetails detail in listOrder)
            {
                orderDAO.updateQuantity(detail.id_Product, detail.buy_Quantity);
            }
            return RedirectToAction("Seller_orderWaitforConfirm", "Seller_OrderManage");
        }
        //Chấp nhận hóa đơn (chức năng này của seller)
        public IActionResult acceptOrder(string id, string status)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            orderDAO.updateOrder(id, status);
            Order order = orderDAO.getOrderbyID(id);
            AccountDAO.InsertNotification(order.id_Customer, "Đơn hàng "+id+" của bản đã được xác nhận");
            return RedirectToAction("Seller_orderWaitforConfirm", "Seller_OrderManage");
        }
        // chi tiết của 1 hóa đơn
        public IActionResult Order_Details(string id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            Order order = orderDAO.getOrderbyID(id);
            return View(order);
        }

    }
}
