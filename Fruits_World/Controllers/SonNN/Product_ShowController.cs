﻿using Fruits_World.Models.DuyenPD.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Product_ShowController : Controller
    {
        ProductDAO dao = new ProductDAO();
        //Hiển thị toàn bộ sản phẩm có tại web
        public IActionResult Product_Show()
        {
            return RedirectToAction("Search", "FindProduct");
        }
    }
}
