﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;

namespace Fruits_World.Controllers.SonNN
{
    public class Seller_StatisticsController : Controller
    {
        AccountDAO AccountDAO = new AccountDAO();
        ProductDAO ProductDAO = new ProductDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //danh sach cus mua tai shop
        public IActionResult Seller_whoBuyAtShop()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            var list = AccountDAO.getAllAccountBuyMostAtOneShop(HttpContext.Session.GetString("id"));
            return View(list);
        }
        // San pham ban chay nhat
        public IActionResult Seller_productSoldAtShop()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }
            var list = ProductDAO.getAllProductSoldAtOneShop(HttpContext.Session.GetString("id"));
            return View(list);
        }
        //Hiển thị doanh thu của 1 shop và lợi nhuận 3% cho admin theo từng tháng
        public IActionResult Seller_Revenue(int page = 1)
        {

            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }
            DateTime dateTime = DateTime.Now;       
            string idAccount = HttpContext.Session.GetString("id");


            List<long> listtien = new List<long>();


                for (int month = 1; month < 13; month++)
                {
                    DateTime date = new DateTime(dateTime.Year + 1 - page, month, 1);
                    var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddTicks(-1);
                    
                    long Revenue = (long)AccountDAO.getRevenue(idAccount, firstDayOfMonth.ToString(), lastDayOfMonth.ToString());
                    listtien.Add(Revenue);   
  
            }
            ViewBag.year = dateTime.Year + 1 - page;
            ViewBag.test = listtien;
            ViewBag.Page = page;
            return View();
        }
    }
}
