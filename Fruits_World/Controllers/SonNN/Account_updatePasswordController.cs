﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Account_updatePasswordController : Controller
    {
        AccountDAO dao = new AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //Get thay đổi mật khẩu của tài khoản
        public IActionResult account_UpdatePassword(string? id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getAccountbyID(id);
            return View(infor);
        }

        //Post thay đổi mật khẩu nếu mật khẩu cũ chính xác và mật khẩu mới, nhập lại mật khẩu mới khớp nhau
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult account_UpdatePassword(string? id, string oldPassword, string password)
        {
            if (ModelState.IsValid)
            {
                string checkoldpass = HttpContext.Session.GetString("password");

                if (AccountDAO.GetMD5(oldPassword).Equals(checkoldpass))
                {
                    dao.updatePassword(id, password);
                    TempData["changePassOk"] = "Bạn đã thay đổi mật khẩu thành công, vui lòng đăng nhập lại!";
                    return RedirectToAction("Login", "Login");
                }
                else
                {
                    ViewBag.Wrongpass = "Mật khẩu cũ không đúng!";
                    return View();
                }
            }
            return View();
        }
    }
}
