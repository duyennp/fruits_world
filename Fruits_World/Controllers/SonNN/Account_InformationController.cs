﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Account_InformationController : Controller
    {
        AccountDAO dao = new AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();
        //Lấy thông của 1 tài khoản thông qua id tài khoản
        public IActionResult accountInformation(string? id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }
            ViewBag.loiquantity = TempData["NotEnoughInformation"];
            ViewBag.Mess = TempData["Mess"];
            ViewBag.loipay = TempData["PayFail"];
            id = HttpContext.Session.GetString("id");
            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getAccountbyID(id);
            ViewBag.idInfo = id;
            return View(infor);
        }
        //Hiển thị thông tin của 1 tài khoản chỉ để xem
        public IActionResult account_Information(string id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }
            var infor = dao.getAccountbyID(id);
            return View(infor);
        }


    }
}
