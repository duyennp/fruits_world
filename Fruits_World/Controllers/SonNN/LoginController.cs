﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class LoginController : Controller
    {
        AccountDAO dao = new AccountDAO();

        //Đăng nhập kiểm tra tài khoản và mật khẩu khớp với một tài khoản đã được tạo trong database
        public IActionResult Login(string username, string password)
        {
            ViewBag.ok = TempData["changePassOk"];
            if (ModelState.IsValid)
            {
                var check = dao.checkLogin(username, password);
                if (check != null)
                {
                    HttpContext.Session.SetString("username", check.fullName);
                    HttpContext.Session.SetString("id", check.id_Account);
                    HttpContext.Session.SetString("password", check.password);
                    HttpContext.Session.SetString("avatar", check.avatar);
                    HttpContext.Session.SetString("role", check.id_Role.ToString());
                    HttpContext.Session.SetInt32("role", Convert.ToInt32(check.id_Role));
                    int? role = HttpContext.Session.GetInt32("role");
                    
                    if (role == 0){
                        return RedirectToAction("BestSaller", "StatisticsOfAdmin");

                    }
                    else if(role == 1)
                    {
                        return RedirectToAction("Seller_ProductShow", "Seller_ProductManage");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                   

                }
                else
                {
                    ViewBag.error = "Tài khoản hoặc mật khẩu không đúng!";
                    return View();
                }
            }
            return View();
        }
        //Đăng xuất xóa hết session
        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Login");
        }
    }
}
