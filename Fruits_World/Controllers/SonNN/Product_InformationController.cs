﻿using Fruits_World.Models.Entity;
using Fruits_World.Models.SonNN.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Product_InformationController : Controller
    {
        ProductDAO dao = new ProductDAO();
        Models.DuyenPD.DAO.FeedbackDAO feedbackDAO = new Models.DuyenPD.DAO.FeedbackDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //Hiển thị thông tin của sản phẩm đã chọn
        public IActionResult product_Information(string? id, int page = 1)
        {
            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getProductbyId(id);
            HttpContext.Session.SetString("idP", infor.id_Product);

            var listFeedback = feedbackDAO.getFeedbackByID(id);
            if (listFeedback.Count != 0)
            {
                ViewBag.listFeedback = listFeedback;
            }
            double mediumStars = feedbackDAO.GetMediumStarsByID(id);
            int intactStars = (int)(mediumStars / 1);
            int haftStars = 0;
            if (mediumStars % 1 != 0)
            {
                haftStars++;
            }
            int loseStars = 5 - intactStars - haftStars;
            ViewBag.intactStars = intactStars;
            ViewBag.haftStars = haftStars;
            ViewBag.loseStars = loseStars;


            // phân trang feedback
            int count = listFeedback.Count; //lấy tổng số product trong list
            int quantityProductDisphay = 10; // số lượng hiển thị ra 

            int endPage = count / quantityProductDisphay; // tổng số trang
            if (count % quantityProductDisphay != 0)
            {
                endPage++;// nếu thiếu trang thì cộng 1
            }

            List<Feedback> listFeedbackEnd = feedbackDAO.GetListFeedbackPaging(listFeedback, page, quantityProductDisphay);
            if (listFeedbackEnd.Count != 0)
                ViewBag.listFeedbackEnd = listFeedbackEnd;

            ViewBag.endPage = endPage;
            ViewBag.Page = page;

            return View(infor);
        }
    }
}
