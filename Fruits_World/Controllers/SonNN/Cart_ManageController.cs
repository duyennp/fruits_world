﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Cart_ManageController : Controller
    {
        CartDAO dao = new CartDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //Hiện thị thông tin giỏ hàng của account thông qua id
        public IActionResult Cart_Show()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return RedirectToAction("Login", "Login");
            }

            if (HttpContext.Session.GetString("id") == null || HttpContext.Session.GetString("id") == "0")
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.listMessage = TempData["listMessage"];
            return View();
        }
        //Thêm sản phẩm vào giỏ hàng của id tài khoản đã đăng nhập
        public IActionResult addtoCart(Product product)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            dao.addtoCart(Convert.ToInt32(HttpContext.Session.GetString("id")), Convert.ToInt32(product.id_Product), Convert.ToInt32(product.productQuantity));
            return RedirectToAction("Cart_Show");
        }
        //Xóa sản phẩm đã chọn ra khỏi giỏ hàng
        public IActionResult DeleteCart(string id, string idsp)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            dao.deleteCart(id, idsp);
            return RedirectToAction("Cart_Show");

        }

        //cập nhật số lượng giỏ hàng
        public IActionResult updateCart(string id, string idsp,string quantity)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            Cart cart = new Cart();           
            cart.id_Account = id;
            cart.id_Product = idsp;
            cart.quantity = Convert.ToInt32(quantity);
            dao.updateCart(cart);       
            return RedirectToAction("Cart_Show");
        }
        public IActionResult Cart_Success()
        {
            return View();
        }

    }
}
