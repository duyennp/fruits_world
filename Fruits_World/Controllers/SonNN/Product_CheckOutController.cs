﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Product_CheckOutController : Controller
    {
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();
        ProductDAO ProductDAO = new ProductDAO();

        //Hiển thị những sản phẩm chuẩn bị thanh toán
        public IActionResult checkOut_Show()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            ProductDAO Productdao = new ProductDAO();
            CartDAO Cartdao = new CartDAO();
            List<string> listnotEnoughProduct = new List<string>();
            string idAccount = HttpContext.Session.GetString("id");
            List<Account> listName = Cartdao.getAllShopIDForCart(idAccount);
            List<Cart> list = null;
            ViewBag.NotEnoughInformation = TempData["NotEnoughInformation"];
            foreach (Account a in listName)
            {
                list = Cartdao.getAllCart(idAccount, a.id_Account);
                foreach (Cart cart in list)
                {
                    Product product = Productdao.getProductbyId(cart.id_Product);
                    if (cart.quantity > product.productQuantity)
                    {
                        listnotEnoughProduct.Add(product.productName);
                    }

                }
            }
            if (listnotEnoughProduct.Count > 0)
            {
                TempData["listMessage"] = listnotEnoughProduct;
                return RedirectToAction("Cart_Show", "Cart_Manage");

            }

            ViewBag.PayFail = TempData["PayFail"];
            return View();
        }
        //Thanh toán những sản phẩm trong giỏ hàng
        public IActionResult checkOut_Pay(string? phuongthucthanhtoan, string address, string sdt, string fullname)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            CartDAO Cartdao = new CartDAO();
            ProductDAO Productdao = new ProductDAO();
            AccountDAO accountDAO = new AccountDAO();
            String idAccount = HttpContext.Session.GetString("id");
            List<Account> listName = Cartdao.getAllShopIDForCart(idAccount);
            List<Cart> list = null;
            TempData["address"] = address;
            TempData["sdt"] = sdt;
            TempData["fullname"] = fullname;
            if (address.Equals("0") || sdt.Equals("0"))
            {
                TempData["NotEnoughInformation"] = "Qúy khách cần nhập địa chỉ và số điện thoại để tiến hành đặt hàng";
                return RedirectToAction("checkOut_Show");
            }

            if (phuongthucthanhtoan.Equals("1"))
            {

                foreach (Account a in listName)
                {
                    list = Cartdao.getAllCart(idAccount, a.id_Account);
                    Cartdao.payCart(idAccount, a.id_Account, phuongthucthanhtoan, list);
                    accountDAO.InsertNotification(a.id_Account, "Bạn có đơn hàng đang chờ xác nhận");

                }
            }
            else if (phuongthucthanhtoan.Equals("2"))
            {
                Account account = accountDAO.getAccountbyID(idAccount);
                int cartPrice = Cartdao.getPriceforCart(idAccount);
                if (account.money < cartPrice)
                {

                    TempData["PayFail"] = "Ví điện tử của quý khách có số dư không đủ! hãy nạp thêm tiền hoặc chọn thanh toán trực tiếp";
                    return RedirectToAction("checkOut_Show");

                }
                else
                {
                    foreach (Account a in listName)
                    {
                        list = Cartdao.getAllCart(idAccount, a.id_Account);
                        Cartdao.payCart(idAccount, a.id_Account, phuongthucthanhtoan, list);
                        accountDAO.InsertNotification(a.id_Account, "Bạn có đơn hàng đang chờ xác nhận");

                    }
                    accountDAO.updateMoney(idAccount, -cartPrice);
                }
            }



            return RedirectToAction("Cart_Success", "Cart_Manage");
        }
        // hiển thị sản phẩm mua ngay
        public IActionResult checkOut_MuaNgay(string idsp, string soluong)
        {
            Product product = ProductDAO.getProductbyId(idsp);
            ViewBag.soluong = soluong;

            return View(product);
        }
        //thanh toán sản phẩm đã chọn
        public IActionResult checkOut_PayMuaNgay(string address, string sdt, string? phuongthucthanhtoan, string productid, string soluong, string tong)
        {
            List<Cart> list = new List<Cart>();
            CartDAO cartDAO = new CartDAO();
            ProductDAO productDAO = new ProductDAO();
            String idAccount = HttpContext.Session.GetString("id");
            AccountDAO accountDAO = new AccountDAO();
            Product product = productDAO.getProductbyId(productid);
            List<string> listnotEnoughProduct = new List<string>();

            if (address.Equals("0") || sdt.Equals("0"))
            {
                TempData["NotEnoughInformation"] = "Qúy khách cần nhập địa chỉ và số điện thoại để tiến hành đặt hàng";
                return RedirectToAction("accountInformation", "Account_Information");
            }
            if (phuongthucthanhtoan.Equals("2"))
            {
                list.Add(new Cart
                {
                    id_Account = idAccount,
                    id_Product = productid,
                    quantity = Convert.ToInt32(soluong)
                });
                cartDAO.payCartMuaNgay(idAccount, product.id_Account, phuongthucthanhtoan, list);
                accountDAO.InsertNotification(product.id_Account, "Bạn có đơn hàng đang chờ xác nhận");

            }
            else if (phuongthucthanhtoan.Equals("1"))
            {
                Account account = accountDAO.getAccountbyID(idAccount);
                if (account.money < Convert.ToInt32(tong))
                {

                    TempData["PayFail"] = "Ví điện tử của quý khách có số dư không đủ! hãy nạp thêm tiền hoặc chọn thanh toán trực tiếp";
                    return RedirectToAction("accountInformation", "Account_Information");


                }
                else
                {
                    list.Add(new Cart
                    {
                        id_Account = idAccount,
                        id_Product = productid,
                        quantity = Convert.ToInt32(soluong)
                    });
                    cartDAO.payCart(idAccount, product.id_Account, phuongthucthanhtoan, list);
                    accountDAO.InsertNotification(product.id_Account, "Bạn có đơn hàng đang chờ xác nhận");


                    accountDAO.updateMoney(idAccount, -Convert.ToInt32(tong));
                }
            }
            return RedirectToAction("Cart_Success", "Cart_Manage");
        }

    }
}
