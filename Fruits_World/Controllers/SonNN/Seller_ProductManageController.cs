﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Seller_ProductManageController : Controller
    {
        ProductDAO dao = new ProductDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();
        AccountDAO AccountDAO = new AccountDAO();
        private readonly IWebHostEnvironment iwebHostEnvironment;//Lấy môi trường folder để lưu file vào

        public Seller_ProductManageController(IWebHostEnvironment webHostEnvironment)
        {
            iwebHostEnvironment = webHostEnvironment;
        }
        //Hiển thị toàn bộ sản phẩm của 1 shop theo id tài khoản của shop
        public IActionResult Seller_ProductShow(int id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            id = Convert.ToInt32(HttpContext.Session.GetString("id"));
            var list = dao.getAllProductforSeller(id, 2);
            return View(list);
        }
        //danh sách trái cây chờ admin xác nhận
        public IActionResult Seller_ProductNeedConfirm(int id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            id = Convert.ToInt32(HttpContext.Session.GetString("id"));
            var list = dao.getAllProductforSeller(id,1);
            return View(list);
        }

         // Get Cập nhật thông tin của 1 sản phẩm của shop đó đã chọn
        public IActionResult Seller_updateProductInformation(string? id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getProductbyId(id);
            return View(infor);
        }
        // Post Cập nhật thông tin của 1 sản phẩm của shop đó đã chọn
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Seller_updateProductInformation(Product product,IFormFile avatar)
        {
            Account AdminAccount = AccountDAO.getAccountbyUsername("admin123");
            int check = 0;
            if(avatar == null)
            {
                Product pro = dao.getProductbyId(product.id_Product);
                product.productImage = pro.productImage;
                AccountDAO.InsertNotification(HttpContext.Session.GetString("id"),"Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!");
                AccountDAO.InsertNotification(AdminAccount.id_Account, "Một sản phẩm đang chò xác nhận!");
                dao.updateFruit(product);
                return RedirectToAction("Seller_ProductShow");
            }
            else
            {
                string text = Path.GetExtension(avatar.FileName);
                if (text.ToLower().EndsWith(".png") || text.ToLower().EndsWith(".jpg"))
                {
                    string folder = "Images/";
                    folder += Guid.NewGuid().ToString() + "_" + avatar.FileName;
                    string a = folder;
                    product.productImage = "/" + a;
                    AccountDAO.InsertNotification(HttpContext.Session.GetString("id"), "Bạn đã gửi yêu cầu chỉnh sửa sản phẩm thành công, vui lòng đợi admin duyệt!");
                    AccountDAO.InsertNotification(AdminAccount.id_Account, "Một sản phẩm đang chò xác nhận!");
                    dao.updateFruit(product);
                    string serverFolder = Path.Combine(iwebHostEnvironment.WebRootPath, folder);
                    avatar.CopyTo(new FileStream(serverFolder, FileMode.Create));
                    return RedirectToAction("Seller_ProductShow");


                }
                else
                {
                    ViewBag.test = "Bạn chỉ có thể chọn file .png hoặc .jpg";
                    return View(product);
                }
            }
            return View(product);

        }
        // Get Cập nhật thông tin sản phẩm
        public IActionResult Seller_updateProduct(string? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var infor = dao.getProductbyId(id);
            return View(infor);
        }

        // Post Cập nhật thông tin sản phẩm
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Seller_updateProduct(Product product)
        {
            if (ModelState.IsValid)
            {
                dao.updatePriceQuantityProduct(product);
                return RedirectToAction("Seller_ProductShow");


            }
            return View();
        }
        //Xóa sản phẩm đó của shop
        public IActionResult deleteProduct(int idsp)
        {
            dao.deleteProduct(idsp);
            return RedirectToAction("Seller_ProductShow");
        }
    }
}
