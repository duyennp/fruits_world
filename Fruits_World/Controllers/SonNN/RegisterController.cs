﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class RegisterController : Controller
    {
        AccountDAO dao = new AccountDAO();
        //get: Đăng kí account
        public IActionResult Register()
        {
            return View();
        }
        //Post Đăng kí account nếu thông tin hợp lệ và tài khoản không trùng vời các người dùng khác sẽ tạo mới account vào database
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(Account account)
        {

            
            if (ModelState.IsValid)
            {
                var check = dao.checkAccountExist(account.username);
                if (check == null)
                {
                    dao.createAccount(account);
                    Account a = dao.getAccountbyUsername(account.username);
                    dao.InsertNotification(a.id_Account, "Bạn đã đăng kí tài khoản thành công, chúc bạn mua sắm tại shop vui vẻ ♥");
                    return RedirectToAction("Login", "Login");
                }
                else
                {
                    ViewBag.error = "Tài Khoản Tồn Tại";
                    return View();
                }

            }
            return View(account);
        }

    }
}
