﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class Seller_OrderManage : Controller

    {
        OrderDAO orderDAO = new OrderDAO();
        AccountDAO AccountDAO = new AccountDAO();
        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //Hiển thị các hóa đơn đang chờ seller xác nhận (chức năng của seller)
        public IActionResult Seller_orderWaitforConfirm()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            return View();
        }
        //Hiển thị hóa đơn đã bán
        public IActionResult Seller_orderProductReceived()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            return View();
        }
        //hiển thị hóa đơn đang vận chuyển
        public IActionResult Seller_orderDelivering()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            return View();
        }
        //hiển thị hóa đơn đã hủy
        public IActionResult Seller_orderCancelled()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            return View();
        }
        //hiển thị chi tiết 1 hóa đơn
        public IActionResult Seller_OrderDetails(string id)
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isLogin(role))
            {
                return NotFound();
            }

            Order order = orderDAO.getOrderbyID(id);
            return View(order);
        }
    }
}
