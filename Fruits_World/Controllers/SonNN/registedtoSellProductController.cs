﻿using Fruits_World.Models.SonNN.DAO;
using FruitsWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fruits_World.Controllers.SonNN
{
    public class registedtoSellProductController : Controller
    {
        ProductDAO dao = new ProductDAO();
        private readonly IWebHostEnvironment iwebHostEnvironment;//Lấy môi trường folder để lưu file vào

        public registedtoSellProductController(IWebHostEnvironment webHostEnvironment)
        {
            iwebHostEnvironment = webHostEnvironment;
        }

        Models.DuyenPD.DAO.DecentralizationDAO phanquyen = new Models.DuyenPD.DAO.DecentralizationDAO();

        //Hiển thị form đăng kí sản phẩm
        public IActionResult registedtoSellProduct()
        {
            int? role = HttpContext.Session.GetInt32("role");
            if (!phanquyen.isSaller(role))
            {
                return NotFound();
            }

            string id = HttpContext.Session.GetString("id");
            ViewBag.idAcc = id;
            return View();
        }

        //Kiểm tra thông tin đăng kí sản phẩm đã nhập vào và lưu hình ảnh sản phẩm của shop đã chọn vào database /wwwroot/Images
        [HttpPost]
        public IActionResult registedtoSellProduct(Product product, IFormFile avatar)
        {
            AccountDAO accountDAO = new AccountDAO();
            Account Adminaccount = accountDAO.getAccountbyUsername("admin123");
            string text = Path.GetExtension(avatar.FileName);
            if (text.ToLower().EndsWith(".png") || text.ToLower().EndsWith(".jpg"))
            {
                string folder = "Images/";
                folder += Guid.NewGuid().ToString() + "_" + avatar.FileName;
                string a = folder;
                ViewBag.test = "Bạn đã đăng kí bán sản phẩm thành công, vui lòng đợi admin duyệt!";
                product.productImage = "/" + a;
                dao.createFruit(product);
                accountDAO.InsertNotification(Adminaccount.id_Account, "Một Sản phẩm cần xác nhận đăng bán!");
                string serverFolder = Path.Combine(iwebHostEnvironment.WebRootPath, folder);
                avatar.CopyTo(new FileStream(serverFolder, FileMode.Create));
                string id = HttpContext.Session.GetString("id");
                ViewBag.idAcc = id;
                return View();


            }
            else
            {
                ViewBag.test = "Bạn chỉ có thể chọn file .png hoặc .jpg";
                string id = HttpContext.Session.GetString("id");
                ViewBag.idAcc = id;
                return View();
            }
        }


    }
}
