﻿namespace FruitsWorld.Models
{
    public class Cart
    {
        public string? id_Account { get; set; }
        public string? id_Product { get; set; }
        public int? quantity { get; set; }

    }
}
