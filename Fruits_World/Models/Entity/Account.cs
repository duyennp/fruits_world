﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FruitsWorld.Models
{
    public class Account
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public string? id_Account { get; set; }
        public string? username { get; set; }
        public string? oldPassword { get; set; }
        public string? password { get; set; }
        public string? ConfirmPassword { get; set; }
        public string? fullName { get; set; }
        public int? birth { get; set; }
        public string? sdt { get; set; }
        public string? address { get; set; }
        public int? money { get; set; }
        public int? id_Role { get; set; }
        public int? isDelete { get; set; }
        public string? avatar { get; set; }
        public int? Money_buyAtShop { get; set; }
        public int? Money_buyAtWeb { get; set; }

    }
}
