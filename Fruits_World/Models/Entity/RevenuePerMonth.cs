﻿namespace FruitsWorld.Models
{
    public class RevenuePerMonth
    {
        public string? Day { get; set; }
        public string? Month { get; set; }
        public string? Year { get; set; }
        public string? Revenue { get; set; }
        public string? AmountforAdmin { get; set; }

    }
}
