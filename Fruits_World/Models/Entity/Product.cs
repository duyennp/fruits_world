﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FruitsWorld.Models
{
    public class Product
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public string? id_Product { get; set; }
        public string? id_Account { get; set; }
        public string? productName { get; set; }
        public string? productImage { get; set; }
        public string? productDescription { get; set; }

        public int? productPrice { get; set; }
        public int? productQuantity { get; set; }
        public int? id_Status { get; set; }

        public int? Quantity_sold { get; set; }
        public int? income { get; set; }
    }
}
