﻿using FruitsWorld.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fruits_World.Models.Entity
{
    public class Feedback
    {
        public string? id_Account { get; set; }
        public string? id_Product { get; set; }
        public int? stars { get; set; }
        public string? content { get; set; }
        public DateTime? date { get; set; }
    }
}
