﻿namespace Fruits_World.Models
{
    public class OrderDetails
    {
        public string? id_Order { get; set; }
        public string? id_Product { get; set; }
        public int? buy_Quantity { get; set; }
        public int? buy_Price { get; set; }
        public Boolean? isFeedback { get; set; }

    }
}
