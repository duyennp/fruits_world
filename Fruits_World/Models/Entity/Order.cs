﻿namespace FruitsWorld.Models
{
    public class Order
    {
        public string? id_Order { get; set; }
        public string? id_Customer { get; set; }
        public string? id_Seller { get; set; }

        public DateTime? dateTime { get; set; }
        public string? id_Status { get; set; }
        public string? id_PaymentMethod { get; set; }
    }
}
