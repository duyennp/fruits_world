﻿using Microsoft.Data.SqlClient;
using FruitsWorld.Models;
using System.Data;

namespace Fruits_World.Models.ThachBN.DAO
{
    public class OrderDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";
       
        //lay don hang cho cus
        public List<Order> getOrderIDforCustomer(string? id, int idstatus)
        {
            List<Order> list = new List<Order>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();

                    cmd.CommandText = "select ID_HoaDon,ID_TaiKhoanSale from HoaDon, ThongTinTaiKhoan where ID_TaiKhoanCus = @id and ThongTinTaiKhoan.ID_TaiKhoan = HoaDon.ID_TaiKhoanSale and HoaDon.ID_TrangThaiDonHang = " + idstatus + " order by ID_HoaDon desc";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Order
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Seller = dr["ID_TaiKhoanSale"].ToString()


                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lay chi tiet don hang theo id hoa don
        public List<OrderDetails> getAllOrderDetailsbyIDOrder(string idhoadon)
        {
            List<OrderDetails> list = new List<OrderDetails>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select ID_HoaDon, ID_SanPham,[SoLuongMua(KG)], [GiaMua(VND)], isFeedback from ChiTietHoaDon where  ID_HoaDon = @idhoadon";
                    cmd.Parameters.AddWithValue("@idhoadon", idhoadon);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new OrderDetails
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Product = dr["ID_SanPham"].ToString(),
                            buy_Quantity = Convert.ToInt32(dr["SoLuongMua(KG)"]),
                            buy_Price = Convert.ToInt32(dr["GiaMua(VND)"]),
                            isFeedback = Convert.ToBoolean(dr["isFeedback"]),

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
    }
}
