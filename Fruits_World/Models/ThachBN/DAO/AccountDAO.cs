﻿using Microsoft.Data.SqlClient;
using FruitsWorld.Models;
using System.Data;

namespace Fruits_World.Models.ThachBN.DAO
{
    public class AccountDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";
        
        //cập nhật vai trò khách hàng
        public void updateRole(string id, int role)
        {
            Fruits_World.Models.SonNN.DAO.AccountDAO accountDAO = new Fruits_World.Models.SonNN.DAO.AccountDAO();
            if (role == 2)
            {
                accountDAO.InsertNotification(id, "Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!");
            }
            else if (role == 1)
            {
                accountDAO.InsertNotification(id, "Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.");
            }
            else
            {
                accountDAO.InsertNotification(id, "Admin không duyệt cho bạn trở thành người hàng, vui lòng kiểm tra lại thông tin tài khoản có phù hợp không.");
            }

            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE ThongTinTaiKhoan set ID_Role = @role where ID_TaiKhoan = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@role", role);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //lay danh sach dang ky ban hang
        public List<Account> getAllAccountRegistedtoSeller()
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where ID_Role = 2";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        // lay acc theo id
        public Account getAccountbyID(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where ID_TaiKhoan = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString()

                        };
                        connection.Close();

                        return a;
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }

        //lay shop theo id product
        public Account getShopbyIDProduct(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {

                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select ThongTinTaiKhoan.Ten,ThongTinTaiKhoan.ID_TaiKhoan,ThongTinTaiKhoan.Avt from ThongTinTaiKhoan,SanPham where ThongTinTaiKhoan.ID_TaiKhoan = SanPham.ID_TaiKhoan and SanPham.ID_SanPham = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            avatar = dr["Avt"].ToString()

                        };
                        connection.Close();

                        return a;
                    }
                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }

        //lay danh sach acc mua nhieu nhat
        public List<Account> getAllAccountBuyMostAtWeb()
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select ThongTinTaiKhoan.ID_TaiKhoan, Sum(ChiTietHoaDon.[GiaMua(VND)] * ChiTietHoaDon.[SoLuongMua(KG)]) as totalBuy " +
                        "from ThongTinTaiKhoan,HoaDon,ChiTietHoaDon " +
                        "where NgayLapHoaDon between GETDATE()-30 and GETDATE() " +
                        "and ThongTinTaiKhoan.ID_TaiKhoan = HoaDon.ID_TaiKhoanCus " +
                        "and HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                        "and HoaDon.ID_TrangThaiDonHang = 3 " +
                        "Group by ThongTinTaiKhoan.ID_TaiKhoan " +
                        "order by Sum(ChiTietHoaDon.[GiaMua(VND)] * ChiTietHoaDon.[SoLuongMua(KG)]) desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            Money_buyAtWeb = Convert.ToInt32(dr["totalBuy"])


                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lay danh sach id account trung binh sao trong 30 ngay tot nhat
        public List<Account> IDSallerBestStars()
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select sp.ID_TaiKhoan,  CAST(sum(SaoCuaSanPham) as float) / CAST(count(*) as float)   as Rating  " +
                        "from DanhGia dg, SanPham sp " +
                        "where dg.ID_SanPham = sp.ID_SanPham " +
                        "and sp.ID_TrangThaiSanPham = 2 " +
                        "and dg.NgayDanhGia between GETDATE()-30 and GETDATE() " +
                        "group by sp.ID_TaiKhoan " +
                        "order by Rating desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            password = dr["Rating"].ToString() // trung binh sao, bien pwd là biến lưu tạm
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lay trung binh sao trong 30 theo id Shop
        public string GetAverageStarsByID(string id)
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select CAST(sum(SaoCuaSanPham) as float) / CAST(count(*) as float)   as Rating  " +
                        "from DanhGia dg, SanPham sp " +
                        "where dg.ID_SanPham = sp.ID_SanPham " +
                        "and sp.ID_TrangThaiSanPham = 2 " +
                        "and sp.ID_TaiKhoan = " + id + " " +
                        "and dg.NgayDanhGia between GETDATE()-30 and GETDATE() " +
                        "group by sp.ID_TaiKhoan " +
                        "order by Rating desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        connection.Close();
                        double sum = Convert.ToDouble(dr["Rating"]);
                        sum = Math.Truncate(sum * 10) / 10;
                        return sum.ToString(); // trung binh sao, bien pwd là biến lưu tạm      
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return null;
        }

        //lay danh sach saller co trung bình sao cao nhat trong 30 ngày
        public List<Account> GetSallerBestStars()
        {
            List<Account> listID = IDSallerBestStars();
            List<Account> listSaller = new List<Account>();
            foreach (Account account in listID)
            {
                listSaller.Add(getAccountbyID(account.id_Account));
            }

            foreach (Account acc in listSaller)
            {
                acc.password = GetAverageStarsByID(acc.id_Account);
            }

            return listSaller;
        }
    }
}
