﻿using Microsoft.Data.SqlClient;
using FruitsWorld.Models;
using System.Data;


namespace Fruits_World.Models.ThachBN.DAO
{
    public class ProductDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";

        //update trang thai trái cay
        public void updateStatusFruit(string id, int status)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE SanPham set ID_TrangThaiSanPham = @status where ID_SanPham = " + id;
                cmd.Parameters.AddWithValue("@status", status);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //xoa san pham
        public void deleteProduct(string idsp)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE SanPham set SoLuongSanPham = 0, ID_TrangThaiSanPham = 3 where ID_SanPham = " + idsp;
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //lay san pham theo trang thai
        public List<Product> getAllProductbyStatus(int id)
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_TrangThaiSanPham = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lay san pham cho shop
        public List<Product> getAllProductofShop(string id)
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_TrangThaiSanPham = 2 and ID_TaiKhoan = " + id + " order by ID_SanPham desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        //danh sach san pham sap xep theo gia tien
        public List<Product> getAllProductofShopSortPrice(string id,string type)
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_TrangThaiSanPham = 2 and ID_TaiKhoan = " + id + " order by [GiaSanPham(VND)] " + type;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lay so luong san pham theo id san pham
        public int getQuantityProduct(string? id)
        {
            int quantity = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select SoLuongSanPham from SanPham, SanPhamTrongGioHang where SanPhamTrongGioHang.ID_SanPham = SanPham.ID_SanPham and SanPham.ID_SanPham = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        quantity = Convert.ToInt32(dr["SoLuongSanPham"]);
                    }
                    connection.Close();

                    return quantity;

                }
                finally
                {
                    connection.Close();

                }
            }

            return quantity;
        }

        //update don hang
        public void updateOrder(string id, string status)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Update HoaDon set ID_TrangThaiDonHang = @status where ID_HoaDon = " + id;
                cmd.Parameters.AddWithValue("@status", status);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //lay san pham theo id
        public Product getProductbyId(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_SanPham = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Product product = new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])
                        };
                        connection.Close();

                        return product;
                    }

                }
                finally
                {
                    connection.Close();

                }

            }

            return null;
        }
    }

}
