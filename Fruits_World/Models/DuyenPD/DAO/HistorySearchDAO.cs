﻿using FruitsWorld.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Fruits_World.Models.DuyenPD.DAO
{
    public class HistorySearchDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";

        // lấy danh sách lịch sử tìm kiếm dựa trên id tài khoản
        public List<SearchHistoty> ListSearchHistory(string idAcc)
        {
            List<SearchHistoty> list = new List<SearchHistoty>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select * from LichSuTimKiem where ID_TaiKhoan = @idAcc order by ID_TimKiem desc";
                cmd.Parameters.AddWithValue("@idAcc", idAcc);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                foreach (DataRow dr in dataTable.Rows)
                {
                    list.Add(new SearchHistoty
                    {
                        id_timkiem = Convert.ToInt32(dr["ID_TimKiem"]),
                        id_Account = Convert.ToInt32(dr["ID_TaiKhoan"]),
                        Noidung = dr["NoiDung"].ToString()
                    });
                }

                }
                finally
                {
                    connection.Close();

                }
            }
            return list;
        }
    }
}
