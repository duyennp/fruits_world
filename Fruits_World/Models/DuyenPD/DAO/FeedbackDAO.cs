﻿using Fruits_World.Models.Entity;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace Fruits_World.Models.DuyenPD.DAO
{
    public class FeedbackDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";

        // feedback thông qua id tàio khoản, id hoá đơn, id sản phẩm, số sao và nội dung feedback
        public void FeedbackProduct(string idAcc, string idBill, string idPro, string starts, string contentFeedback)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "insert into danhgia values (" + idAcc + "," + idPro + "," + starts + ",N'" + contentFeedback + "',@Date) update ChiTietHoaDon set isFeedback = 1 where ID_HoaDon = " + idBill + " and ID_SanPham = " + idPro;
                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //lay danh sach feedback của 1 sản phẩm theo id sản phẩm
        public List<Feedback> getFeedbackByID(string id)
        {
            List<Feedback> list = new List<Feedback>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from DanhGia where ID_SanPham = " + id + " order by NgayDanhGia desc ";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);

                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Feedback
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            id_Product = dr["ID_SanPham"].ToString(),
                            stars = Convert.ToInt32(dr["SaoCuaSanPham"]),
                            content = dr["DanhGiaSanPham"].ToString(),
                            date = Convert.ToDateTime(dr["NgayDanhGia"])
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return list;
        }

        // lấy số sao đánh giá của 1 sản phẩm qua id
        public double GetStarsByID(string id)
        {
            int sum = 0;
            DbConnection connection = new SqlConnection(con);
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select sum(SaoCuaSanPham) as total from DanhGia where ID_SanPham = " + id;

                var reader = command.ExecuteReader(); // Thực thi Query

                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    if (reader.IsDBNull(0))
                    {
                        connection.Close(); // Đóng kết nối sau khi chạy xong
                        return 0;
                    }
                    sum = reader.GetInt32(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong

            return sum;
        }

        // lấy số đánh giá của 1 sản phẩm qua id san pham
        public double GetQuantityFeedbackByID(string id)
        {
            int sum = 0;
            DbConnection connection = new SqlConnection(con);
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select count(ID_SanPham) as total from DanhGia where ID_SanPham = " + id;

                var reader = command.ExecuteReader(); // Thực thi Query

                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    if (reader.IsDBNull(0))
                    {
                        connection.Close(); // Đóng kết nối sau khi chạy xong
                        return 0;
                    }
                    sum = reader.GetInt32(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong

            return sum;
        }

        // lấy trung bình sao của sản phẩm
        public double GetMediumStarsByID(string id)
        {
            double sum = 0.0;
            double total = GetStarsByID(id);
            double quantity = GetQuantityFeedbackByID(id);
            if (quantity < 1)
            {
                return 5;
            }
            sum = total / quantity;
            sum = Math.Truncate(sum * 10) / 10;

            return sum;
        }

        // lấy số đánh giá của 1 shop qua id acc 
        public double GetTotalFeedbackByID(string id)
        {
            int sum = 0;
            DbConnection connection = new SqlConnection(con);
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select count(*) from SanPham sp, DanhGia dg where sp.ID_SanPham = dg.ID_SanPham and sp.ID_TaiKhoan = " + id;

                var reader = command.ExecuteReader(); // Thực thi Query

                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    if (reader.IsDBNull(0))
                    {
                        connection.Close(); // Đóng kết nối sau khi chạy xong
                        return 0;
                    }
                    sum = reader.GetInt32(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong

            return sum;
        }

        //phan trang danh sach feedback 
        public List<Feedback> GetListFeedbackPaging(List<Feedback> list, int page, int quantity)
        {
            List<Feedback> listFeedback = new List<Feedback>();

            int starts = (page - 1) * quantity;
            int end = starts + quantity;
            for (int i = starts; i < end && i < list.Count; i++)
            {
                listFeedback.Add(list[i]);
            }
            return listFeedback;
        }
    }
}
