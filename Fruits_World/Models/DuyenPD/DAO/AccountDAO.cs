﻿using Microsoft.Data.SqlClient;
using FruitsWorld.Models;
using System.Data;
using System.Data.Common;

namespace Fruits_World.Models.DuyenPD.DAO
{
    public class AccountDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";
        Fruits_World.Models.SonNN.DAO.AccountDAO accountDAO = new Fruits_World.Models.SonNN.DAO.AccountDAO();
        ProductDAO ProductDAO = new ProductDAO();
        //lay danh sach account hien len web
        public List<Account> getAllAccount()
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where isDelete = 0 and ID_Role != 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString()

                        });
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            return list;
        }

        //lay thong tin account theo id
        public Account getAccountbyID(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where ID_TaiKhoan = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString()
                        };
                        connection.Close();
                        return a;
                    }
                }
                finally
                {
                    connection.Close();
                }

            }

            return null;
        }

        //update role cho account qua id va role truyền vào
        public void updateRole(string id, int role)
        {
            Account Adminaccount = accountDAO.getAccountbyUsername("admin123");
            if (role == 2)
            {
                accountDAO.InsertNotification(id, "Bạn đã đăng kí để trở thành người bán hàng, vui lòng chờ admin duyệt!");
                accountDAO.InsertNotification(Adminaccount.id_Account, "Có người dùng đăng kí thành người bán hàng!");
            }
            else if (role == 1)
            {

                accountDAO.InsertNotification(id, "Bạn đã trở thành người bán hàng, bây giờ bạn có thể đăng bán sản phẩm tại Fruits World.");
            }
            else
            {
                accountDAO.InsertNotification(id, "Admin không duyệt cho bạn trở thành người hàng, vui lòng kiểm tra lại thông tin tài khoản có phù hợp không.");
            }
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE ThongTinTaiKhoan set ID_Role = @role where ID_TaiKhoan = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@role", role);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        // update tiền cho account
        public void updateMoney(string id, int money)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE ThongTinTaiKhoan set Tien = Tien + @money where ID_TaiKhoan = " + id;
                cmd.Parameters.AddWithValue("@money", money);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //update thong tin acc
        public void updateInformation(Account account)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "update ThongTinTaiKhoan set Ten = @name,NamSinh = @birth, SDT = @sdt, DiaChi=@address where ID_TaiKhoan = " + account.id_Account;
                cmd.Parameters.AddWithValue("@name", account.fullName);
                cmd.Parameters.AddWithValue("@birth", account.birth);
                cmd.Parameters.AddWithValue("@sdt", account.sdt);
                cmd.Parameters.AddWithValue("@address", account.address);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        // xoa account
        public void deleteAccount(int id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "update ThongTinTaiKhoan set isDelete = 1 where ID_TaiKhoan =" + id;
                deleteProduct(id);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //xoa product
        public void deleteProduct(int id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE SanPham set SoLuongSanPham = 0, ID_TrangThaiSanPham = 3 where ID_TaiKhoan = " + id;
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        // lấy danh sách account là sale theo searchContent
        public List<Account> ListAccountSearchByName(string searchContent)
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where Ten like N'%" + searchContent + "%' and isDelete = 0 and ID_Role = 1";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar= dr["Avt"].ToString()

                        });
                    }

                }
                finally
                {
                    connection.Close();
                }
            }
            return list;
        }

        // lấy tên account theo id account
        public string GetAccountNameByID(string id)
        {
            Account account = getAccountbyID(id);
            if (account != null) return account.fullName;
            return null;
        }

        // lấy username account theo id account
        public string GetAccountUserNameByID(string id)
        {
            Account account = getAccountbyID(id);
            if (account != null) return account.username;
            return null;
        }

        // lấy avt account theo id account
        public string GetAccountAvtByID(string id)
        {
            Account account = getAccountbyID(id);
            if (account != null) return account.avatar;
            return null;
        }

        // lấy tổng số doanh thu khoang thoi gian 
        public List<Account> ListTotalRevenueOfSaller(DateTime start, DateTime end)
        {
            List<Account> list = new List<Account>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();

                    cmd.CommandText = "select tt.ID_TaiKhoan, tt.TaiKhoan, tt.MatKhau, tt.Ten, tt.NamSinh, tt.SDT, tt.DiaChi, tt.tien, tt.ID_Role, tt.isDelete, tt.Avt , Sum(cthd.[GiaMua(VND)] * cthd.[SoLuongMua(KG)]) as total " +
                        "from ChiTietHoaDon cthd, HoaDon hd, ThongTinTaiKhoan tt  " +
                        "where cthd.ID_HoaDon = hd.ID_HoaDon " +
                        "and hd.ID_TaiKhoanSale = tt.ID_TaiKhoan  " +
                        "and tt.isDelete = 0  and hd.ID_TrangThaiDonHang = 3  " +
                        "and NgayLapHoaDon between @start and @end " +
                        "group by tt.ID_TaiKhoan, tt.TaiKhoan, tt.MatKhau, tt.Ten, tt.NamSinh, tt.SDT, tt.DiaChi, tt.tien, tt.ID_Role, tt.isDelete, tt.Avt " +
                        "order by Sum(cthd.[GiaMua(VND)] * cthd.[SoLuongMua(KG)]) desc";
                    cmd.Parameters.AddWithValue("@start", start);
                    cmd.Parameters.AddWithValue("@end", end);

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = (Convert.ToInt32(dr["total"]) * 3 / 100).ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString(),
                            oldPassword = dr["total"].ToString()
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return list;
        }

        //lay trung binh sao theo id Shop
        public string GetAverageStarsByID(string id)
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select sp.ID_TaiKhoan,  CAST(sum(SaoCuaSanPham) as float) / CAST(count(*) as float) as Rating " +
                        "from DanhGia dg, SanPham sp  " +
                        "where dg.ID_SanPham = sp.ID_SanPham " +
                        "and sp.ID_TrangThaiSanPham = 2  " +
                        "and sp.ID_TaiKhoan = "+id+" " +
                        "group by sp.ID_TaiKhoan " +
                        "order by Rating desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        connection.Close();
                        double sum = Convert.ToDouble(dr["Rating"]);
                        sum = Math.Truncate(sum * 10) / 10;
                        return sum.ToString(); // trung binh sao, bien pwd là biến lưu tạm      
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return null;
        }
    }
}
