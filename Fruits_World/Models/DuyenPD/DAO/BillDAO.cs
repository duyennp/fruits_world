﻿using FruitsWorld.Models;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace Fruits_World.Models.DuyenPD.DAO
{
    public class BillDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";

        // lấy danh sách hoá đơn
        public List<Order> ListBill()
        {
            List<Order> list = new List<Order>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from HoaDon order by ID_HoaDon desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Order
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Customer = dr["ID_TaiKhoanCus"].ToString(),
                            id_Seller = dr["ID_TaiKhoanSale"].ToString(),
                            dateTime = Convert.ToDateTime(dr["NgayLapHoaDon"]),
                            id_Status = dr["ID_TrangThaiDonHang"].ToString(),
                            id_PaymentMethod = dr["ID_PhuongThucThanhToan"].ToString()
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return list;
        }

        //lấy tên trạng thái của đơn hàng thông qua id trạng thái của đơn hàng
        public string GetStatusNameOfBill(string idStatus)
        {
            string statusBill = "";
            DbConnection connection = new SqlConnection(con); // Mở kết nối đến database
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select TenTrangThaiDonHang from TrangThaiDonHang where ID_TrangThaiDonHang = " + idStatus;

                var reader = command.ExecuteReader(); // Thực thi Query

                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    if (reader.IsDBNull(0)) return "khong co du lieu";
                    statusBill = "" + reader.GetString(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong
            return statusBill;
        }

        //lấy tên phương thức thanh toán của đơn hàng thông qua id phương thức thanh toán của đơn hàng
        public string GetStatusNameOfPayment(string idPayment)
        {
            string statusPayment = "";
            DbConnection connection = new SqlConnection(con); // Mở kết nối đến database
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select TenPhuongThucThanhToan from PhuongThucThanhToan where ID_PhuongThucThanhToan = " + idPayment;

                var reader = command.ExecuteReader(); // Thực thi Query

                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    if (reader.IsDBNull(0)) return "khong co du lieu";
                    statusPayment = reader.GetString(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong
            return statusPayment;
        }

        // lấy danh sách sản phẩm dựa trên id hoá đơn
        public List<OrderDetails> ListProductByIDBill(string idBill)
        {
            List<OrderDetails> list = new List<OrderDetails>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();

                    cmd.CommandText = "select * from ChiTietHoaDon where ID_HoaDon = " + idBill;

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new OrderDetails
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Product = dr["ID_SanPham"].ToString(),
                            buy_Quantity = Convert.ToInt32(dr["SoLuongMua(KG)"]),
                            buy_Price = Convert.ToInt32(dr["GiaMua(VND)"]),
                            isFeedback = Convert.ToBoolean(dr["isFeedback"])
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        // lấy danh sách sản phẩm đã mua và đã nhận hàng của tài khoản
        public List<OrderDetails> BuyHistoryProduct(string idAcc)
        {
            List<OrderDetails> list = new List<OrderDetails>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();

                cmd.CommandText = "select ct.ID_HoaDon, ct.ID_SanPham, ct.[SoLuongMua(KG)], ct.[GiaMua(VND)], ct.isFeedback " +
                    "from ChiTietHoaDon ct, HoaDon hd " +
                    "where ct.ID_HoaDon = hd.ID_HoaDon " +
                    "and hd.ID_TrangThaiDonHang = 3 " +
                    "and hd.ID_TaiKhoanCus = " + idAcc;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);


                foreach (DataRow dr in dataTable.Rows)
                {
                    list.Add(new OrderDetails
                    {
                        id_Order = dr["ID_HoaDon"].ToString(),
                        id_Product = dr["ID_SanPham"].ToString(),
                        buy_Quantity = Convert.ToInt32(dr["SoLuongMua(KG)"]),
                        buy_Price = Convert.ToInt32(dr["GiaMua(VND)"]),
                        isFeedback = Convert.ToBoolean(dr["isFeedback"])
                    });
                }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
    }
}
