﻿namespace Fruits_World.Models.DuyenPD.DAO
{
    public class DecentralizationDAO
    {
        // phân quyền là admin
        public bool isAdmin(int? role)
        {
            if (role == 0) return true;
            else return false;
        }

        //phân quyền là saller
        public bool isSaller(int? role)
        {
            if (role == 1) return true;
            else return false;
        }

        public bool isLogin(int? role)
        {
            if (role != null)
            {
                return true;
            }
            else return false;
        }
    }
}
