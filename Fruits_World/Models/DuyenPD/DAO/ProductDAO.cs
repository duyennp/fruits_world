﻿using FruitsWorld.Models;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace Fruits_World.Models.DuyenPD.DAO
{
    public class ProductDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";



        // lấy danh sách sản phẩm dựa trên min max
        public List<Product> ListProductByOptionPrice(int min, int max)
        {
            List<Product> list = new List<Product>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    if (max == 0)
                    {
                        cmd.CommandText = "select * from SanPham where [GiaSanPham(VND)] >= " + min + " and ID_TrangThaiSanPham = 2";

                    }
                    else cmd.CommandText = "select * from SanPham where [GiaSanPham(VND)] >= " + min + " and [GiaSanPham(VND)] <= " + max + " and ID_TrangThaiSanPham = 2";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        // lấy tổng số sản phẩm của 1 shop
        public string GetTotalProductsByIDAcc(string id)
        {
            string sum = "0";
            DbConnection connection = new SqlConnection(con);
            // Mở kết nối đến database
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select count(ID_SanPham) as sumOfPro from SanPham where ID_TrangThaiSanPham = 2 and ID_TaiKhoan = " + id;

                var reader = command.ExecuteReader(); // Thực thi Query
                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    sum = "" + reader.GetInt32(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong

            return sum;

        }

        // lấy tổng số sản phẩm đã bán của 1 product của 1 shop by id product
        public string GetNumberOfProductsSold(string id)
        {
            string sum = "0";
            DbConnection connection = new SqlConnection(con);
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select sum([SoLuongMua(KG)]) as soluongmua from ChiTietHoaDon, HoaDon where ChiTietHoaDon.ID_HoaDon = HoaDon.ID_HoaDon and HoaDon.ID_TrangThaiDonHang = 3 and ID_SanPham = " + id;

                var reader = command.ExecuteReader(); // Thực thi Query

                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    if (reader.IsDBNull(0))
                    {
                        connection.Close(); // Đóng kết nối sau khi chạy xong
                        return "0";
                    }
                    sum = "" + reader.GetInt32(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong

            return sum;
        }

        //list id product được bán nhiều nhất
        public List<Product> GetListIDProductSoldMost(DateTime? date)
        {
            List<Product> list = new List<Product>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    if (date != null)
                    {
                        cmd.CommandText = "select SanPham.ID_SanPham " +
                            "from SanPham,HoaDon,ChiTietHoaDon " +
                            "where HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                            "and SanPham.ID_SanPham = ChiTietHoaDon.ID_SanPham " +
                            "and SanPham.ID_TrangThaiSanPham = 2 " +
                            "and HoaDon.ID_TrangThaiDonHang = 3 " +
                            "and NgayLapHoaDon between @date and GETDATE()" +
                            "Group by SanPham.ID_SanPham " +
                            "order by Sum([SoLuongMua(KG)]) desc";
                        cmd.Parameters.AddWithValue("@date", DateTime.Today.AddMonths(-1));
                    }
                    else
                    {
                        cmd.CommandText = "select SanPham.ID_SanPham " +
                            "from SanPham,HoaDon,ChiTietHoaDon " +
                            "where HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                            "and SanPham.ID_SanPham = ChiTietHoaDon.ID_SanPham " +
                            "and SanPham.ID_TrangThaiSanPham = 2" +
                            "and HoaDon.ID_TrangThaiDonHang = 3 " +
                            "Group by SanPham.ID_SanPham " +
                            "order by Sum([SoLuongMua(KG)]) desc";
                    }

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString()
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //list id product chưa bán được
        public List<Product> GetListIDProductSoldLeast()
        {
            List<Product> list = new List<Product>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();

                    cmd.CommandText = "select SanPham.ID_SanPham into #tam " +
                        "from SanPham,HoaDon,ChiTietHoaDon " +
                        "where HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                        "and SanPham.ID_SanPham = ChiTietHoaDon.ID_SanPham " +
                        "and SanPham.ID_TrangThaiSanPham = 2 " +
                        "and HoaDon.ID_TrangThaiDonHang = 3 " +
                        "Group by SanPham.ID_SanPham " +
                        "order by Sum([SoLuongMua(KG)]) desc " +
                        "select ID_SanPham from SanPham where ID_TrangThaiSanPham = 2 " +
                        "EXCEPT " +
                        "select * from #tam " +
                        "drop table #tam";

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString()
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        // láy product by id 
        public Product getProductbyId(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_SanPham = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Product product = new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])
                        };
                        connection.Close();

                        return product;
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }

        // lấy danh sách sản phẩm ban chay
        public List<Product> GetListProductSoldMost(DateTime? date)
        {
            List<Product> listID = new List<Product>(); ;
            if (date == null)
            {
                listID = GetListIDProductSoldMost(null);
            }
            else
            {
                listID = GetListIDProductSoldMost(date);
            }

            List<Product> listIDSoldLeast = GetListIDProductSoldLeast();
            List<Product> listPro = new List<Product>();
            foreach (Product product in listID)
            {
                listPro.Add(getProductbyId(product.id_Product));
            }
            foreach (Product product in listIDSoldLeast)
            {
                listPro.Add(getProductbyId(product.id_Product));
            }
            return listPro;
        }

        //lay ten san pham theo id
        public string GetProductNameByID(string id)
        {
            var pro = getProductbyId(id);
            if (pro != null) return pro.productName;
            return null;
        }

        //lấy tổng số sản phẩm trên web
        public int GetTotalProduct()
        {
            int total = 0;
            DbConnection connection = new SqlConnection(con);
            // Mở kết nối đến database
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                // Câu truy vấn SQL
                command.CommandText = "select count(*) as total from SanPham where ID_TrangThaiSanPham = 2";

                var reader = command.ExecuteReader(); // Thực thi Query
                while (reader.Read()) // Kiểm tra kết quả trả về
                {
                    total = reader.GetInt32(0);
                }
            }
            connection.Close(); // Đóng kết nối sau khi chạy xong

            return total;
        }

        //lay danh sach san pham the page
        public List<Product> GetListProductPaging(List<Product> list, int page, int quantity)
        {
            List<Product> listPro = new List<Product>();

            int starts = (page - 1) * quantity;
            int end = starts + quantity;
            for (int i = starts; i < end && i < list.Count; i++)
            {
                listPro.Add(list[i]);
            }
            return listPro;
        }
    }
}
