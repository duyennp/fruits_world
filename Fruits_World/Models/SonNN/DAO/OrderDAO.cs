﻿using FruitsWorld.Models;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Fruits_World.Models.SonNN.DAO
{
    public class OrderDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";

        //Lấy danh sách id hóa đơn cho để hiển thị danh sách
        public List<Order> getOrderIDforCustomer(string? id, int idstatus)
        {
            List<Order> list = new List<Order>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();

                    cmd.CommandText = "select * from HoaDon where ID_TaiKhoanCus = @id and ID_TrangThaiDonHang = @idstatus order by ID_HoaDon desc;";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@idstatus", idstatus);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Order
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Customer = dr["ID_TaiKhoanCus"].ToString(),
                            id_Seller = dr["ID_TaiKhoanSale"].ToString(),
                            dateTime = Convert.ToDateTime(dr["NgayLapHoaDon"]),
                            id_Status = dr["ID_TrangThaiDonHang"].ToString(),
                            id_PaymentMethod = dr["ID_PhuongThucThanhToan"].ToString()
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        //Lấy 1 hóa đơn dựa trên id hóa đơn
        public Order getOrderbyID(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from HoaDon where ID_HoaDon = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Order a = new Order
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Customer = dr["ID_TaiKhoanCus"].ToString(),
                            id_Seller = dr["ID_TaiKhoanSale"].ToString(),
                            dateTime = Convert.ToDateTime(dr["NgayLapHoaDon"]),
                            id_Status = dr["ID_TrangThaiDonHang"].ToString(),
                            id_PaymentMethod = dr["ID_PhuongThucThanhToan"].ToString()

                        };
                        connection.Close();

                        return a;
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }
        //Lấy danh sách id hóa đơn cho để hiển thị danh sách
        public List<Order> getOrderIDforSeller(string? id, int idstatus)
        {
            List<Order> list = new List<Order>();

            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();

                    cmd.CommandText = "select * from HoaDon where ID_TaiKhoanSale = @id and ID_TrangThaiDonHang = @idstatus order by ID_HoaDon desc;";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@idstatus", idstatus);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Order
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Customer = dr["ID_TaiKhoanCus"].ToString(),
                            id_Seller = dr["ID_TaiKhoanSale"].ToString(),
                            dateTime = Convert.ToDateTime(dr["NgayLapHoaDon"]),
                            id_Status = dr["ID_TrangThaiDonHang"].ToString(),
                            id_PaymentMethod = dr["ID_PhuongThucThanhToan"].ToString()


                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        //Lấy chi tiết hóa đơn của 1 hóa đơn dựa tren id hóa đơn
        public List<OrderDetails> getAllOrderDetailsbyIDOrder(string idhoadon)
        {
            List<OrderDetails> list = new List<OrderDetails>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ChiTietHoaDon where  ID_HoaDon = @idhoadon";
                    cmd.Parameters.AddWithValue("@idhoadon", idhoadon);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new OrderDetails
                        {
                            id_Order = dr["ID_HoaDon"].ToString(),
                            id_Product = dr["ID_SanPham"].ToString(),
                            buy_Quantity = Convert.ToInt32(dr["SoLuongMua(KG)"]),
                            buy_Price = Convert.ToInt32(dr["GiaMua(VND)"]),
                            isFeedback = Convert.ToBoolean(dr["isFeedback"]),


                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        //Cập nhật trạng thái của hóa đơn (chờ xác nhận, đã xác nhận, đã nhận, hủy)
        public void updateOrder(string id, string status)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Update HoaDon set ID_TrangThaiDonHang = @status where ID_HoaDon = " + id;
                cmd.Parameters.AddWithValue("@status", status);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //Cập nhật số lượng của sản phẩm
        public void updateQuantity(string idsp, int? quantity)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Update SanPham set SoLuongSanPham = SoLuongSanPham + @quantity where ID_SanPham = " + idsp;
                cmd.Parameters.AddWithValue("@quantity", quantity);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //Lấy số tiền của 1 hóa đơn
        public int getPriceofOrder(string? idhoadon)
        {
            int price = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select SUM([SoLuongMua(KG)] * [GiaMua(VND)]) as tonghoadon from HoaDon, ChiTietHoaDon where HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon and HoaDon.ID_HoaDon = " + idhoadon;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        price = Convert.ToInt32(dr["tonghoadon"]);
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return price;
        }
    }
}
