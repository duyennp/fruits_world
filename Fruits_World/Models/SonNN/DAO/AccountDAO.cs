﻿using FruitsWorld.Models;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.Text;

namespace Fruits_World.Models.SonNN.DAO
{
    public class AccountDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";

        //Tạo 1 tài khoản mới
        public void createAccount(Account account)
        {
            string defaultAvatar = "/Images/0030b3f7-1068-4e18-9bdb-7037a2003d6f_ava mac dinh.jpg";
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "insert into ThongTinTaiKhoan values(@user,@pass,@name,0,0,0,0,3,0,@avatar)";
                cmd.Parameters.AddWithValue("@user", account.username);
                cmd.Parameters.AddWithValue("@pass", GetMD5(account.password));
                cmd.Parameters.AddWithValue("@name", account.fullName);
                cmd.Parameters.AddWithValue("@avatar", defaultAvatar);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //Kiểm tra đăng nhập dựa trên tài khoản và mật khẩu
        public Account checkLogin(string username, string password)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where TaiKhoan = @user and MatKhau = @pass and isDelete = 0";
                    cmd.Parameters.AddWithValue("@user", username);
                    cmd.Parameters.AddWithValue("@pass", GetMD5(password));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString()

                        };
                        connection.Close();

                        return a;
                    }

                }
                finally
                {
                    connection.Close();

                }

            }

            return null;
        }

        //Lấy 1 account dựa trên id
        public Account getAccountbyID(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where ID_TaiKhoan = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString()

                        };
                        connection.Close();

                        return a;
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }

        //lấy 1 account dựa trên username
        public Account getAccountbyUsername(string user)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where TaiKhoan = @user";
                    cmd.Parameters.AddWithValue("@user", user);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"]),
                            avatar = dr["Avt"].ToString()

                        };
                        connection.Close();

                        return a;
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }

        // cập nhật tiền cho account
        public void updateMoney(string id, int money)
        {

            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE ThongTinTaiKhoan set Tien = Tien + @money where ID_TaiKhoan = " + id;
                cmd.Parameters.AddWithValue("@money", money);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //check tên tài khoản khi đăng kí có tồn tại hay không
        public Account checkAccountExist(string user)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongTinTaiKhoan where TaiKhoan = @user";
                    cmd.Parameters.AddWithValue("@user", user);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Account a = new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            username = dr["TaiKhoan"].ToString(),
                            password = dr["MatKhau"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            birth = Convert.ToInt32(dr["NamSinh"]),
                            sdt = dr["SDT"].ToString(),
                            address = dr["DiaChi"].ToString(),
                            money = Convert.ToInt32(dr["Tien"]),
                            id_Role = Convert.ToInt32(dr["ID_Role"]),
                            isDelete = Convert.ToInt32(dr["isDelete"])
                        };
                        connection.Close();

                        return a;

                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return null;
        }

        //cập nhật thông tin account
        public void updateInformation(Account account)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "update ThongTinTaiKhoan set Ten = @name,NamSinh = @birth, SDT = @sdt, DiaChi=@address where ID_TaiKhoan = " + account.id_Account;
                cmd.Parameters.AddWithValue("@name", account.fullName);
                cmd.Parameters.AddWithValue("@birth", account.birth);
                cmd.Parameters.AddWithValue("@sdt", account.sdt);
                cmd.Parameters.AddWithValue("@address", account.address);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //cập nhật mật khẩu tài khoản
        public void updatePassword(string id, string password)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "update ThongTinTaiKhoan set MatKhau = @pass where ID_TaiKhoan = " + id;
                cmd.Parameters.AddWithValue("@pass", GetMD5(password));
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //cập nhập hình đại diện
        public void updateAvatar(string id, string avatar)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "update ThongTinTaiKhoan set Avt = @avatar where ID_TaiKhoan = " + id;
                cmd.Parameters.AddWithValue("@avatar", avatar);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //thêm 1 thông báo vào 1 tài khoản
        public void InsertNotification(string id, string noidung)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into ThongBao values(@id,@noidung, GETDATE(),0)";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@noidung", noidung);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //cập nhật thông báo thành đã xem
        public void UpdateNotification(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Update ThongBao set isWatch = 1 where ID_TaiKhoan = " + id;
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //lấy số thông báo của 1 account
        public int getNumberNotification(string id)
        {
            int number = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "Select Count(*) as number  from ThongBao where isWatch = 0 and ID_TaiKhoan = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        number = Convert.ToInt32(dr["number"]);
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return number;
        }

        //lay danh sach acc mua nhiều nhất tại 1 shop
        public List<Account> getAllAccountBuyMostAtOneShop(string idShop)
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select ThongTinTaiKhoan.Avt,ThongTinTaiKhoan.ID_TaiKhoan,ThongTinTaiKhoan.Ten ,Sum(ChiTietHoaDon.[GiaMua(VND)] * ChiTietHoaDon.[SoLuongMua(KG)]) as totalBuy " +
                        "from ThongTinTaiKhoan,HoaDon,ChiTietHoaDon " +
                        "where ThongTinTaiKhoan.ID_TaiKhoan = HoaDon.ID_TaiKhoanCus " +
                        "and HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                        "and HoaDon.ID_TrangThaiDonHang = 3 " +
                        "and HoaDon.ID_TaiKhoanSale = @idshop " +
                        "Group by ThongTinTaiKhoan.Avt,ThongTinTaiKhoan.ID_TaiKhoan, ThongTinTaiKhoan.Ten " +
                        "order by Sum(ChiTietHoaDon.[GiaMua(VND)] * ChiTietHoaDon.[SoLuongMua(KG)]) desc;";
                    cmd.Parameters.AddWithValue("@idshop", idShop);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            fullName = dr["Ten"].ToString(),
                            avatar = dr["Avt"].ToString(),
                            Money_buyAtShop = Convert.ToInt32(dr["totalBuy"])


                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lay doanh thu của shop
        public int? getRevenue(string? id, string firstdayofMonth, string lastdayofMonth)
        {
            int? price = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "SELECT SUM([SoLuongMua(KG)] * [GiaMua(VND)]) as tongdoanhthu FROM HoaDon, ChiTietHoaDon where HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon and HoaDon.ID_TrangThaiDonHang = 3 and HoaDon.ID_TaiKhoanSale = @id and HoaDon.NgayLapHoaDon BETWEEN @first AND @last";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@first", firstdayofMonth);
                    cmd.Parameters.AddWithValue("@last", lastdayofMonth);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        if (dr["tongdoanhthu"] == DBNull.Value)
                        {
                            connection.Close();

                            return 0;
                        }
                        price = Convert.ToInt32(dr["tongdoanhthu"]);
                    }

                }
                finally
                {
                    connection.Close();

                }
            }
            return price;
        }

        //lấy danh sách thong bao theo của 1 account
        public List<Notification> getAllNotification(string id)
        {
            List<Notification> list = new List<Notification>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from ThongBao where ID_TaiKhoan = " + id + " order by NgayThongBao desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Notification
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            Noidung = dr["NoiDung"].ToString(),
                            dateTime = Convert.ToDateTime(dr["NgayThongBao"]),
                            isWatch = Convert.ToBoolean(dr["isWatch"])
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //lấy danh sách số năm bán hàng của 1 shop
        public List<int> getAllYear(string id)
        {
            List<int> list = new List<int>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select distinct Year(NgayLapHoaDon) as nam from HoaDon where ID_TrangThaiDonHang = 3 and ID_TaiKhoanSale = " + id;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);


                foreach (DataRow dr in dataTable.Rows)
                {
                    list.Add(Convert.ToInt32(dr["nam"]));
                }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //chuyên chuổi thành mã băm
        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }
    }
}
