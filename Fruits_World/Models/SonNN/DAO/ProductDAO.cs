﻿using FruitsWorld.Models;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Fruits_World.Models.SonNN.DAO
{
    public class ProductDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";
        //Lấy danh sách sản phẩm cho trang chủ
        public List<Product> getProductforIndex()
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select top (10) * from SanPham where ID_TrangThaiSanPham = 2 order by ID_SanPham desc";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }

            }

            return list;
        }
        //Lây danh sách sản phẩm nổi bật
        public List<Product> getHighlightsProduct()
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select top(4) SanPham.ID_SanPham,SanPham.TenSanPham, SanPham.AnhSanPham,SanPham.[GiaSanPham(VND)] " +
                        "from SanPham,HoaDon,ChiTietHoaDon " +
                        "where HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                        "and SanPham.ID_SanPham = ChiTietHoaDon.ID_SanPham " +
                        "and HoaDon.ID_TrangThaiDonHang = 3 " +
                        "and SanPham.ID_TrangThaiSanPham = 2 " +
                        "Group by SanPham.ID_SanPham,SanPham.TenSanPham, SanPham.AnhSanPham,SanPham.[GiaSanPham(VND)] " +
                        "order by sum(ChiTietHoaDon.[GiaMua(VND)] * ChiTietHoaDon.[SoLuongMua(KG)]) desc;";
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productImage = dr["AnhSanPham"].ToString()
                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

        //Tạo 1 sản phẩm mới
        public void createFruit(Product product)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO SanPham values(@id,@name,@image,@infor,@price,@quantity,1)";
                cmd.Parameters.AddWithValue("@id", product.id_Account);
                cmd.Parameters.AddWithValue("@name", product.productName);
                cmd.Parameters.AddWithValue("@image", product.productImage);
                cmd.Parameters.AddWithValue("@infor", product.productDescription);
                cmd.Parameters.AddWithValue("@price", product.productPrice);
                cmd.Parameters.AddWithValue("@quantity", product.productQuantity);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //Lấy 1 sản phẩm theo id
        public Product getProductbyId(string id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_SanPham = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        Product product = new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])
                        };
                        connection.Close();

                        return product;
                    }

                }
                finally
                {
                    connection.Close();

                }

            }

            return null;
        }
        //Cập nhật thông tin sản phẩm
        public void updateFruit(Product product)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE SanPham set TenSanPham = @name, AnhSanPham = @image, ThongTinSanPham = @infor, ID_TrangThaiSanPham = 1 where ID_SanPham = @id";
                cmd.Parameters.AddWithValue("@id", product.id_Product);
                cmd.Parameters.AddWithValue("@name", product.productName);
                cmd.Parameters.AddWithValue("@image", product.productImage);
                cmd.Parameters.AddWithValue("@infor", product.productDescription);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //cập nhật giá số lượng của sản phẩm
        public void updatePriceQuantityProduct(Product product)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE SanPham set [GiaSanPham(VND)] = @price, SoLuongSanPham = @quantity where ID_SanPham = @idsp";
                cmd.Parameters.AddWithValue("@idsp", product.id_Product);
                cmd.Parameters.AddWithValue("@price", product.productPrice);
                cmd.Parameters.AddWithValue("@quantity", product.productQuantity);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //xóa 1 sản phẩm và set số lượng = 0
        public void deleteProduct(int id)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "update SanPham set ID_TrangThaiSanPham = 3, SoLuongSanPham = 0 where ID_SanPham = " + id;
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        //Lấy toàn bộ sản phẩm của 1 shop cho shop đó
        public List<Product> getAllProductforSeller(int id, int idstatus)
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPham where ID_TaiKhoan = " + id + " and ID_TrangThaiSanPham = " + idstatus;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            productDescription = dr["ThongTinSanPham"].ToString(),
                            productPrice = Convert.ToInt32(dr["GiaSanPham(VND)"]),
                            productQuantity = Convert.ToInt32(dr["SoLuongSanPham"]),
                            id_Status = Convert.ToInt32(dr["ID_TrangThaiSanPham"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        // Lấy danh sách sản phẩm bán chạy nhất của 1 shop
        public List<Product> getAllProductSoldAtOneShop(string idShop)
        {
            List<Product> list = new List<Product>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select SanPham.ID_SanPham,SanPham.TenSanPham, SanPham.AnhSanPham ,Sum([SoLuongMua(KG)]) as totalQuantity, sum([GiaMua(VND)]) as totalPrice " +
                        "from SanPham,HoaDon,ChiTietHoaDon where ID_TaiKhoan = @idshop and HoaDon.ID_HoaDon = ChiTietHoaDon.ID_HoaDon " +
                        "and SanPham.ID_SanPham = ChiTietHoaDon.ID_SanPham and HoaDon.ID_TrangThaiDonHang = 3 " +
                        "Group by SanPham.ID_SanPham,SanPham.TenSanPham,SanPham.AnhSanPham order by Sum([SoLuongMua(KG)]) desc;";
                    cmd.Parameters.AddWithValue("@idshop", idShop);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Product
                        {
                            id_Product = dr["ID_SanPham"].ToString(),
                            productName = dr["TenSanPham"].ToString(),
                            productImage = dr["AnhSanPham"].ToString(),
                            Quantity_sold = Convert.ToInt32(dr["totalQuantity"]),
                            income = Convert.ToInt32(dr["totalPrice"])


                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }

    }
}
