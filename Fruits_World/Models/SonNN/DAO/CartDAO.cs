﻿using FruitsWorld.Models;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Fruits_World.Models.SonNN.DAO
{
    public class CartDAO
    {
        string con = "Server=(local);uid=sa;pwd=123456;Database=Fruits_World;Trusted_Connection=True;";
        ProductDAO productDAO = new ProductDAO();
        //Lấy danh sách giỏ hàng của 1 tài khoản
        public List<Cart> getAllCart(string id, string idshop)
        {

            List<Cart> list = new List<Cart>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select SanPhamTrongGioHang.ID_TaiKhoan, SanPhamTrongGioHang.ID_SanPham, [SoLuong(KG)] " +
                        "from ThongTinTaiKhoan, SanPhamTrongGioHang, SanPham " +
                        "where SanPhamTrongGioHang.ID_SanPham = SanPham.ID_SanPham " +
                        "and SanPham.ID_TaiKhoan = ThongTinTaiKhoan.ID_TaiKhoan " +
                        "and SanPhamTrongGioHang.ID_TaiKhoan = @id and ThongTinTaiKhoan.ID_TaiKhoan = " + idshop;
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Cart
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            id_Product = dr["ID_SanPham"].ToString(),
                            quantity = Convert.ToInt32(dr["SoLuong(KG)"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        //Lấy danh sách sản phẩm trong giỏ hàng của 1 tài khoản theo id
        public List<Cart> getAllCartbyID(string id)
        {

            List<Cart> list = new List<Cart>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select * from SanPhamTrongGioHang where ID_TaiKhoan = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Cart
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            id_Product = dr["ID_SanPham"].ToString(),
                            quantity = Convert.ToInt32(dr["SoLuong(KG)"])

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        // cập nhật số lượng sản phẩm trong giỏ hàng
        public void updateCart(Cart cart)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE SanPhamTrongGioHang set [SoLuong(KG)] = @quantity where ID_TaiKhoan = @id and ID_SanPham = " + cart.id_Product;
                cmd.Parameters.AddWithValue("@id", cart.id_Account);
                cmd.Parameters.AddWithValue("@quantity", cart.quantity);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //xóa 1 sản phẩm trong giỏ hàng
        public void deleteCart(string id, string idsp)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "delete from SanPhamTrongGioHang where ID_TaiKhoan = " + id + " and ID_SanPham = " + idsp;
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //Thanh toán toàn bộ sản phẩm trong giỏ hàng
        public void payCart(string idcus, string idsell, string phuongthuc, List<Cart> list)
        {
            int price = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO HoaDon values(" + idcus + "," + idsell + ",GETDATE(),1," + phuongthuc + ")";

                foreach (Cart cart in list)
                {
                    price = getPriceProductbyId(cart.id_Product);
                    cmd.CommandText += "INSERT INTO ChiTietHoaDon values((select top (1) ID_HoaDon from HoaDon order by ID_HoaDon desc)," + cart.id_Product + "," + cart.quantity + "," + price + "," + 0 + ")";
                    updateQuantity(cart.id_Product, -cart.quantity);
                    deleteCart(idcus, cart.id_Product);
                }

                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //thanh toán 1 sản phẩm đã chọn để mua ngay
        public void payCartMuaNgay(string idcus, string idsell, string phuongthuc, List<Cart> list)
        {
            int price = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO HoaDon values(" + idcus + "," + idsell + ",GETDATE(),1," + phuongthuc + ")";

                foreach (Cart cart in list)
                {
                    price = getPriceProductbyId(cart.id_Product);
                    cmd.CommandText += "INSERT INTO ChiTietHoaDon values((select top (1) ID_HoaDon from HoaDon order by ID_HoaDon desc)," + cart.id_Product + "," + cart.quantity + "," + price + "," + 0 + ")";
                    updateQuantity(cart.id_Product, -cart.quantity);
                }

                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        // thêm vào giỏ hàng 1 sản phẩm
        public void addtoCart(int id, int idProduct, int quantity)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "exec CHECKID @id, @idsp, @quantity";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@idsp", idProduct);
                cmd.Parameters.AddWithValue("@quantity", quantity);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        // Lấy tài khoản chủ shop của các sản phẩm đã thêm vào giỏ hàng
        public List<Account> getAllShopIDForCart(string id)
        {
            List<Account> list = new List<Account>();
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select distinct ThongTinTaiKhoan.ID_TaiKhoan, ThongTinTaiKhoan.Ten " +
                        "from ThongTinTaiKhoan, SanPhamTrongGioHang, SanPham " +
                        "where SanPhamTrongGioHang.ID_SanPham = SanPham.ID_SanPham " +
                        "and SanPham.ID_TaiKhoan = ThongTinTaiKhoan.ID_TaiKhoan and SanPhamTrongGioHang.ID_TaiKhoan = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        list.Add(new Account
                        {
                            id_Account = dr["ID_TaiKhoan"].ToString(),
                            fullName = dr["Ten"].ToString()

                        });
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return list;
        }
        // Lấy giá của sản phẩm
        public int getPriceProductbyId(string idProduct)
        {
            int money = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select [GiaSanPham(VND)] from SanPham WHERE ID_SanPham = " + idProduct;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        money = Convert.ToInt32(dr["GiaSanPham(VND)"]);
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return money;
        }
        // lấy số lượng sản phẩm trong giỏ hàng
        public int getNumberofCart(string id)
        {
            int number = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select count(*) as number from SanPhamTrongGioHang where ID_TaiKhoan = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        number = Convert.ToInt32(dr["number"]);
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return number;
        }

       //cập nhật số lượng của sản phẩm
        public void updateQuantity(string idsp, int? quantity)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Update SanPham set SoLuongSanPham = SoLuongSanPham + @quantity where ID_SanPham = " + idsp;
                cmd.Parameters.AddWithValue("@quantity", quantity);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
        //Lấy tổng số tiền của toàn bộ sản phẩm trong giỏ hàng
        public int getPriceforCart(string id)
        {
            int money = 0;
            using (SqlConnection connection = new SqlConnection(con))
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "select Sum(SanPham.[GiaSanPham(VND)] * SanPhamTrongGioHang.[SoLuong(KG)]) as sumAll from SanPhamTrongGioHang, SanPham where SanPhamTrongGioHang.ID_SanPham = SanPham.ID_SanPham and SanPhamTrongGioHang.ID_TaiKhoan = " + id;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);


                    foreach (DataRow dr in dataTable.Rows)
                    {
                        money = Convert.ToInt32(dr["sumAll"]);
                    }

                }
                finally
                {
                    connection.Close();

                }
            }

            return money;
        }

    }
}
